<?php

return [
    'role_structure' => [
        'super_admin' => [
            'years' => 'c,r,u,d',
            'countries' => 'c,r,u,d',
            'camps' => 'c,r,u,d',
            'campStatus' => 'c,r,u,d',
            'tours' => 'c,r,u,d',
            'teams' => 'c,r,u,d',
            'sponsors' => 'c,r,u,d',
            'sponsorShips' => 'c,r,u,d',
            'users' => 'c,r,u,d',
            'plans' => 'c,r,u,d',
            'reports' => 'c,r,u,d',
            'logs' => 'c,r,u,d',
        ],
        'operation' => [
            'years' => 'c,r,u,d',
            'countries' => 'c,r,u,d',
            'cites' => 'c,r,u,d',
            'status' => 'c,r,u,d',
            'tours' => 'c,r,u,d',
            'teams' => 'c,r,u,d',
            'sponsors' => 'c,r,u,d',
            'sponsor_ships' => 'c,r,u,d',
            'users' => 'c,r,u,d',
            'plans' => 'c,r,u,d',
        ],
        'customer_service' => [
            'years' => 'c,r,u,d',
            'countries' => 'c,r,u,d',
            'cites' => 'c,r,u,d',
            'status' => 'c,r,u,d',
            'tours' => 'c,r,u,d',
            'teams' => 'c,r,u,d',
            'sponsors' => 'c,r,u,d',
            'sponsor_ships' => 'c,r,u,d',
            'users' => 'c,r,u,d',

        ],


    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
