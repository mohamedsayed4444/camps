<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('year_id')->unsigned()->nullable();
            $table->bigInteger('tour_id')->unsigned()->nullable();
            $table->bigInteger('country_id')->unsigned()->nullable();
            $table->bigInteger('camp_id')->unsigned()->nullable();
            $table->bigInteger('team_id')->unsigned()->nullable();
            $table->bigInteger('sponsor_id')->unsigned()->nullable();
            $table->bigInteger('camp_status_id')->unsigned()->nullable();
            $table->bigInteger('sponsor_ship_id')->unsigned()->nullable();
            $table->date('from');
            $table->date('to');
            $table->text('notes')->nullable();
            $table->boolean('need_follow_up')->default(true);
            $table->string('type')->nullable();
            $table->timestamps();
            $table->softDeletes();
//            $table->foreign('year_id')->references('id')->on('years');
//            $table->foreign('tour_id')->references('id')->on('tours');
//            $table->foreign('country_id')->references('id')->on('countries');
//            $table->foreign('camp_id')->references('id')->on('camps');
//            $table->foreign('team_id')->references('id')->on('teams');
//            $table->foreign('sponsor_id')->references('id')->on('sponsors');
//            $table->foreign('camp_status_id')->references('id')->on('camp_status');
//            $table->foreign('sponsor_ship_id')->references('id')->on('sponsor_ships');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plans');
    }
}
