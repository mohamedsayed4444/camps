<table id="html5-extension" class="table table-hover non-hover" style="width:100%">
    <thead>
    <tr>
        <th>#</th>
        <th>@lang('site.name_ar')</th>
        <th>@lang('site.name_en')</th>
        <th>@lang('site.status')</th>
        <th>@lang('site.created_at')</th>
        <th>@lang('site.created_by')</th>

        <th>@lang('site.action')</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($tours as $x=>$tour)
        <tr id="row_{{$tour->id}}">
            <td>{{ $x+1}}</td>
            <td><div class="td-content product-brand text-danger">{{ $tour->name_ar }}</div></td>
            <td><div class="td-content product-brand text-warning">{{ $tour->name_en}}</div></td>
            <td><span class="badge {{$tour->status==1?'badge-success':'badge-danger'}}">{{ $tour->status==1?__('site.active'):__('site.inActive') }}</span>
            {{--                        <td>{{ $tour->user->name }}</td>--}}
            <td>{{date('d-m-Y',strtotime($tour->created_at))  }}</td>
            <td><div class="td-content product-brand text-danger">{{ @$tour->user->name }}</div></td>

            <td class="text-center">

                <ul class="table-controls">
                    <li><a href="javascript:void(0)" data-id="{{ $tour->id }}" class="bs-tooltip edit"
                           title="@lang('site.edit') ">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                 stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round"
                                 class="feather feather-check-circle text-primary">
                                <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                <polyline points="22 4 12 14.01 9 11.01"></polyline>
                            </svg>
                        </a></li>
                    <li><a href="#"
                           onclick="sweet_delete( ' {{ route('tours.destroy',$tour->id) }} ' , '{{ trans('site.confirm_delete') }}' ,{{ $tour->id }} )"
                           class="bs-tooltip" title="@lang('site.delete') ">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                 stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round"
                                 class="feather feather-x-circle text-danger">
                                <circle cx="12" cy="12" r="10"></circle>
                                <line x1="15" y1="9" x2="9" y2="15"></line>
                                <line x1="9" y1="9" x2="15" y2="15"></line>
                            </svg>
                        </a></li>

                </ul>


            </td>
        </tr>

    @endforeach

    </tbody>
    <tfoot>
    <tr>
        <th style="width: 20px">@lang('site.order_number')</th>
        <th>@lang('site.name_ar')</th>
        <th>@lang('site.name_en')</th>
        <th>@lang('site.status')</th>
        <th>@lang('site.created_at')</th>
        <th>@lang('site.created_by')</th>
        <th style="visibility: hidden">#</th>


    </tr>
    </tfoot>
</table>
