<!-- Nameen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('NameEn', __('site.NameEn')) !!}
    {!! Form::text('NameEn', null, ['class' => 'form-control','required'=>'required','aria-describedby'=>"inputGroupPrepend" ,'required']) !!}
    <span class="invalid-feedback">@lang('site.required')</span>
    @error('NameEn')<span class="text-danger">{{ $message }}</span>@enderror
    {!! Form::hidden('Tag_ID', null, ['class' => 'form-control']) !!}
</div>

<!-- Namear Field -->
<div class="form-group col-sm-6">
    {!! Form::label('NameAr',__('site.NameAr'))!!}
    {!! Form::text('NameAr', null, ['class' => 'form-control','required'=>'required','aria-describedby'=>"inputGroupPrepend" ,'required']) !!}
    <span class="invalid-feedback">@lang('site.required')</span>
    @error('NameAr')<span class="text-danger">{{ $message }}</span>@enderror
</div>

<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Category', __('site.Category')) !!}
    {!! Form::text('Category', null, ['class' => 'form-control','required'=>'required','aria-describedby'=>"inputGroupPrepend" ,'required']) !!}
    <span class="invalid-feedback">@lang('site.required')</span>
    @error('Category')<span class="text-danger">{{ $message }}</span>@enderror
</div><!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Html Class',__('site.HtmlClass')) !!}
    {!! Form::text('HtmlClass', null, ['class' => 'form-control']) !!}
</div>

