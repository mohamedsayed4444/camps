<!-- Tag Id Field -->
<div class="col-sm-12">
    {!! Form::label('Tag_ID', 'Tag Id:') !!}
    <p>{{ $tag->Tag_ID }}</p>
</div>

<!-- Nameen Field -->
<div class="col-sm-12">
    {!! Form::label('NameEn', 'Nameen:') !!}
    <p>{{ $tag->NameEn }}</p>
</div>

<!-- Namear Field -->
<div class="col-sm-12">
    {!! Form::label('NameAr', 'Namear:') !!}
    <p>{{ $tag->NameAr }}</p>
</div>

<!-- Category Field -->
<div class="col-sm-12">
    {!! Form::label('Category', 'Category:') !!}
    <p>{{ $tag->Category }}</p>
</div>

<!-- Timestamp Field -->
<div class="col-sm-12">
    {!! Form::label('TimeStamp', 'Timestamp:') !!}
    <p>{{ $tag->TimeStamp }}</p>
</div>

<!-- Updatedat Field -->
<div class="col-sm-12">
    {!! Form::label('updatedAt', 'Updatedat:') !!}
    <p>{{ $tag->updatedAt }}</p>
</div>

