@extends('layouts.app')

@section('style')

@endsection
@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <nav class="breadcrumb-two" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">@lang('site.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a
                                href="{{route('tags.index')}}">@lang('site.tags')</a></li>
                        <li class="breadcrumb-item "><a href="#">@lang('site.show')</a></li>
                    </ol>
                </nav>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-primary float-right"
                   href="{{ route('tags.index') }}">
                    @lang('site.tag')
                </a>
            </div>
        </div>
    </div>


    <div class="content px-3 layout-top-spacing">


        <div class="card">
            <div class="card-body">
                <div class="row">
                    @include('tags.show_fields')
                </div>
            </div>
        </div>
    </div>
@endsection
