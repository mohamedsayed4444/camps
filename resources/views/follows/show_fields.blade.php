<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $follow->id }}</p>
</div>

<!-- Camp Id Field -->
<div class="col-sm-12">
    {!! Form::label('camp_id', 'Camp Id:') !!}
    <p>{{ $follow->camp_id }}</p>
</div>

<!-- Camp Status Id Field -->
<div class="col-sm-12">
    {!! Form::label('camp_status_id', 'Camp Status Id:') !!}
    <p>{{ $follow->camp_status_id }}</p>
</div>

<!-- Ending Date Field -->
<div class="col-sm-12">
    {!! Form::label('ending_date', 'Ending Date:') !!}
    <p>{{ $follow->ending_date }}</p>
</div>

<!-- Notes Field -->
<div class="col-sm-12">
    {!! Form::label('notes', 'Notes:') !!}
    <p>{{ $follow->notes }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $follow->status }}</p>
</div>

<!-- Files Field -->
<div class="col-sm-12">
    {!! Form::label('files', 'Files:') !!}
    <p>{{ $follow->files }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $follow->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $follow->updated_at }}</p>
</div>

