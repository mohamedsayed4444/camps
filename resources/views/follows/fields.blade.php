<input type="hidden" value="{{request('type')}}" name="type">

<div class="form-group col-md-4">
    <label>@lang('site.camp')</label>
    <select data-placeholder="@lang('site.camp')" class="form-control basic" name="camp_id"
            aria-describedby="inputGroupPrepend" required>
        <option value="">@lang('site.camp')</option>

        @foreach(\App\Models\Camp::get() as $camp)
            <option
                value="{{$camp->id}} "{{@$follow->camp_id == $camp->id||old('camp_id') == $camp->id? 'selected' : '' }} >{{$camp->name}}</option>
        @endforeach

    </select>
    <span class="invalid-feedback">@lang('site.required')</span>

    @error('camp_id')<span class="text-danger">{{ $message }}</span>@enderror

</div>
<div class="form-group col-md-4">
    <label>@lang('site.camp_status')</label>
    <select data-placeholder="@lang('site.camp_status')" class="form-control basic" name="camp_status_id"
            aria-describedby="inputGroupPrepend" required>
        <option value="">@lang('site.camp_status')</option>

        @foreach(\App\Models\CampStatus::get() as $camp_status)
            <option
                value="{{$camp_status->id}} "{{@$follow->camp_status_id == $camp_status->id||old('camp_status_id') == $camp_status->id? 'selected' : '' }} >{{$camp_status->name}}</option>
        @endforeach

    </select>
    <span class="invalid-feedback">@lang('site.required')</span>

    @error('camp_status_id')<span class="text-danger">{{ $message }}</span>@enderror

</div>


<div class="form-group col-sm-4">
    <label>@lang('site.ending_date')</label>
    {!! Form::date('ending_date', @$follow->ending_date, ['class' => 'form-control']) !!}
</div>

<!-- Notes Field -->
<div class="form-group col-sm-10">
    <label>@lang('site.notes')</label>
    {!! Form::text('notes', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group " style="margin-left: -5px;margin-right: -5px">
    <label>@lang('site.status')</label>
    <div class="col-sm-6">
        <label class="switch s-icons s-outline  s-outline-success  mb-8 mr-4">
            <input type="checkbox" name="status" id="status" {{@$follow->status==1?'checked':''}} >

            <span class="slider round"></span>
        </label>
    </div>
</div>



<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('files', 'Image:') !!}
    <div class="input-group">
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="files"
                    id="file">
            {!! Form::label('files', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group col-sm-5">
        @isset($follow)
            <a href="{{@$follow->files_path}}" class="download_button" target="_blank">
                <div class="downloadicon">
                    <div class="cloud">
                        <div class="arrowdown"></div>
                    </div>
                </div>

                <div class="downloads">{{ __('site.download')}} : <span class="value"></span></div>
            </a>
        @endisset
    </div>

</div>

<div class="clearfix"></div>
