<div class="table-responsive">
    <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('site.type')</th>
            <th>@lang('site.camp')</th>
            <th>@lang('site.campStatus')</th>
            <th>@lang('site.ending_date')</th>
            <th>@lang('site.status')</th>

            <th>@lang('site.action')</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($follows as $x=>$follow)
            <tr id="row_{{$follow->id}}">
                <td>{{ $x+1}}</td>
                <td><div class="td-content product-brand text-secondary">{{ @$follow->type }}</div></td>
                <td><div class="td-content product-brand text-danger">{{ @$follow->camp->name }}</div></td>
                <td><div class="td-content product-brand text-danger">{{ @$follow->campStatus->name }}</div></td>
                <td>{{date('d-m-Y',strtotime($follow->ending_date))  }}</td>
                <td><span class="badge {{$follow->status==1?'badge-success':'badge-danger'}}">{{ $follow->status==1?__('site.active'):__('site.inActive') }}</span>

                <td class="text-center">

                    <ul class="table-controls">
                        <li><a href="{{ route('follows.edit',$follow->id).'?type='.request('type')}}" data-id="{{ $follow->id }}" class="bs-tooltip edit"
                               title="@lang('site.edit') ">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                     stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round"
                                     class="feather feather-check-circle text-primary">
                                    <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                    <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                </svg>
                            </a></li>
                        <li><a href="#"
                               onclick="sweet_delete( ' {{ route('follows.destroy',$follow->id) }} ' , '{{ trans('site.confirm_delete') }}' ,{{ $follow->id }} )"
                               class="bs-tooltip" title="@lang('site.delete') ">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                     stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round"
                                     class="feather feather-x-circle text-danger">
                                    <circle cx="12" cy="12" r="10"></circle>
                                    <line x1="15" y1="9" x2="9" y2="15"></line>
                                    <line x1="9" y1="9" x2="15" y2="15"></line>
                                </svg>
                            </a></li>

                    </ul>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
