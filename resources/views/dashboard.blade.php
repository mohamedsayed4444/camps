@extends('layouts.app')
@section('title')
    @lang('site.dashboard')
@endsection

@section('style')

    <link href="{{ $url}}{{app()->getLocale()}}/assets/css/widgets/modules-widgets.css" rel="stylesheet"
          type="text/css"/>
    <style>
        .widget-account-invoice-two .account-box .info {
            display: flex;
            justify-content: space-between;
            margin-bottom: 30px;
            height: 70px;
            /*margin-bottom: 86px;*/
        }

        #content {
            margin-top: 70px !important;
        }

    </style>
@endsection
@section('content')

    <div class="row layout-top-spacing">
        <div class="row mr-1 ml-1 mt-0">
            @foreach(\App\Models\Follow::all()->groupBy('type') as $follow)
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-account-invoice-two">
                        <div class="widget-content">
                            <div class="account-box">
                                <div class="info">
                                    <div class="inv-title">
                                        <h5 class="">{{__('site.'.$follow[0]->type)}} </h5>
                                    </div>
                                    <div class="inv-balance-info">
                                        <p class="inv-balance">{{$follow->count()}}</p>
                                        <span class="inv-stats balance-credited">@lang('site.follows')</span>
                                    </div>
                                </div>
                                <div class="acc-action">
                                    <div class="">

                                        <a href="{{route('follows.create').'?type='.$follow[0]->type}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24"
                                                 fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round"
                                                 stroke-linejoin="round" class="feather feather-plus">
                                                <line x1="12" y1="5" x2="12" y2="19"></line>
                                                <line x1="5" y1="12" x2="19" y2="12"></line>
                                            </svg>
                                        </a>
                                    </div>
                                    <a href="{{route('follows.index').'?type='.@$follow[0]->type}}">@lang('site.index')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-account-invoice-two">
                    <div class="widget-content">
                        <div class="account-box">
                            <div class="info">
                                <div class="inv-title">
                                    <h5 class="">{{__('site.teams')}} </h5>
                                </div>
                                <div class="inv-balance-info">
                                    <p class="inv-balance">{{\App\Models\Team::count()}}</p>
                                    <span class="inv-stats balance-credited">@lang('site.team')</span>
                                </div>
                            </div>
                            <div class="acc-action">
                                <div class="">

                                    <a href="{{route('teams.index')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-plus">
                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                    </a>
                                </div>
                                <a href="{{route('teams.index')}}">@lang('site.index')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-account-invoice-two">
                    <div class="widget-content">
                        <div class="account-box">
                            <div class="info">
                                <div class="inv-title">
                                    <h5 class="">{{__('site.tours')}} </h5>
                                </div>
                                <div class="inv-balance-info">
                                    <p class="inv-balance">{{\App\Models\Tour::count()}}</p>
                                    <span class="inv-stats balance-credited">@lang('site.tour')</span>
                                </div>
                            </div>
                            <div class="acc-action">
                                <div class="">

                                    <a href="{{route('tours.index')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-plus">
                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                    </a>
                                </div>
                                <a href="{{route('tours.index')}}">@lang('site.index')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-account-invoice-two">
                    <div class="widget-content">
                        <div class="account-box">
                            <div class="info">
                                <div class="inv-title">
                                    <h5 class="">{{__('site.users')}} </h5>
                                </div>
                                <div class="inv-balance-info">
                                    <p class="inv-balance">{{\App\Models\User::count()}}</p>
                                    <span class="inv-stats balance-credited">@lang('site.user')</span>
                                </div>
                            </div>
                            <div class="acc-action">
                                <div class="">

                                    <a href="{{route('users.index')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-plus">
                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                    </a>
                                </div>
                                <a href="{{route('users.index')}}">@lang('site.index')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-account-invoice-two">
                    <div class="widget-content">
                        <div class="account-box">
                            <div class="info">
                                <div class="inv-title">
                                    <h5 class="">{{__('site.years')}} </h5>
                                </div>
                                <div class="inv-balance-info">
                                    <p class="inv-balance">{{\App\Models\Year::count()}}</p>
                                    <span class="inv-stats balance-credited">@lang('site.year')</span>
                                </div>
                            </div>
                            <div class="acc-action">
                                <div class="">

                                    <a href="{{route('years.index')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-plus">
                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                    </a>
                                </div>
                                <a href="{{route('years.index')}}">@lang('site.index')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-account-invoice-two">
                    <div class="widget-content">
                        <div class="account-box">
                            <div class="info">
                                <div class="inv-title">
                                    <h5 class="">{{__('site.sponsors')}} </h5>
                                </div>
                                <div class="inv-balance-info">
                                    <p class="inv-balance">{{\App\Models\Sponsor::count()}}</p>
                                    <span class="inv-stats balance-credited">@lang('site.sponsor')</span>
                                </div>
                            </div>
                            <div class="acc-action">
                                <div class="">

                                    <a href="{{route('sponsors.index')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-plus">
                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                    </a>
                                </div>
                                <a href="{{route('sponsors.index')}}">@lang('site.index')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-account-invoice-two">
                    <div class="widget-content">
                        <div class="account-box">
                            <div class="info">
                                <div class="inv-title">
                                    <h5 class="">{{__('site.sponsorShips')}} </h5>
                                </div>
                                <div class="inv-balance-info">
                                    <p class="inv-balance">{{\App\Models\SponsorShip::count()}}</p>
                                    <span class="inv-stats balance-credited">@lang('site.sponsorShip')</span>
                                </div>
                            </div>
                            <div class="acc-action">
                                <div class="">

                                    <a href="{{route('sponsorShips.index')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-plus">
                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                    </a>
                                </div>
                                <a href="{{route('sponsorShips.index')}}">@lang('site.index')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-account-invoice-two">
                    <div class="widget-content">
                        <div class="account-box">
                            <div class="info">
                                <div class="inv-title">
                                    <h5 class="">{{__('site.camps')}} </h5>
                                </div>
                                <div class="inv-balance-info">
                                    <p class="inv-balance">{{\App\Models\Camp::count()}}</p>
                                    <span class="inv-stats balance-credited">@lang('site.camp')</span>
                                </div>
                            </div>
                            <div class="acc-action">
                                <div class="">

                                    <a href="{{route('camps.index')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-plus">
                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                    </a>
                                </div>
                                <a href="{{route('camps.index')}}">@lang('site.index')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-account-invoice-two">
                    <div class="widget-content">
                        <div class="account-box">
                            <div class="info">
                                <div class="inv-title">
                                    <h5 class="">{{__('site.countries')}} </h5>
                                </div>
                                <div class="inv-balance-info">
                                    <p class="inv-balance">{{\App\Models\Country::count()}}</p>
                                    <span class="inv-stats balance-credited">@lang('site.country')</span>
                                </div>
                            </div>
                            <div class="acc-action">
                                <div class="">

                                    <a href="{{route('countries.index')}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-plus">
                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                        </svg>
                                    </a>
                                </div>
                                <a href="{{route('countries.index')}}">@lang('site.index')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-chart-one">
                <div class="widget-heading">
                    <h5 class="">@lang('site.follows')</h5>
                    <div class="task-action">
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" id="pendingTask"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-more-horizontal">
                                    <circle cx="12" cy="12" r="1"></circle>
                                    <circle cx="19" cy="12" r="1"></circle>
                                    <circle cx="5" cy="12" r="1"></circle>
                                </svg>
                            </a>

                        </div>
                    </div>
                </div>

                <div class="widget-content">
                    <div id="allFollowStatic"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-chart-two">
                <div class="widget-heading">
                    <h5 class="">@lang('site.follows')</h5>
                </div>
                <div class="widget-content">
                    <div id="allCountFollow"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-chart-two">
                <div class="widget-heading">
                    <h5 class="">@lang('site.FinanceFollowUp')</h5>
                </div>
                <div class="widget-content">
                    <div id="financeFollows"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-chart-two">
                <div class="widget-heading">
                    <h5 class="">@lang('site.AdminFollowUp')</h5>
                </div>
                <div class="widget-content">
                    <div id="adminFollows"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-chart-two">
                <div class="widget-heading">
                    <h5 class="">@lang('site.DrugsFollowUp')</h5>
                </div>
                <div class="widget-content">
                    <div id="drugsFollows"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-chart-two">
                <div class="widget-heading">
                    <h5 class="">@lang('site.MediaFollowUp')</h5>
                </div>
                <div class="widget-content">
                    <div id="mediaFollows"></div>
                </div>
            </div>
        </div>


        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-table-two">

                <div class="widget-heading">
                    <h5 class="">Recent Follows</h5>
                </div>

                <div class="widget-content">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>
                                    <div class="th-content">Customer</div>
                                </th>
                                <th>
                                    <div class="th-content">Product</div>
                                </th>
                                <th>
                                    <div class="th-content">Invoice</div>
                                </th>
                                <th>
                                    <div class="th-content th-heading">Price</div>
                                </th>
                                <th>
                                    <div class="th-content">Status</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="td-content customer-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="avatar"><span>Luke Ivory</span></div>
                                </td>
                                <td>
                                    <div class="td-content product-brand text-primary">Headphone</div>
                                </td>
                                <td>
                                    <div class="td-content product-invoice">#46894</div>
                                </td>
                                <td>
                                    <div class="td-content pricing"><span class="">$56.07</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="badge badge-success">Paid</span></div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="td-content customer-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="avatar"><span>Andy King</span></div>
                                </td>
                                <td>
                                    <div class="td-content product-brand text-warning">Nike Sport</div>
                                </td>
                                <td>
                                    <div class="td-content product-invoice">#76894</div>
                                </td>
                                <td>
                                    <div class="td-content pricing"><span class="">$88.00</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="badge badge-primary">Shipped</span></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="td-content customer-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="avatar"><span>Laurie Fox</span></div>
                                </td>
                                <td>
                                    <div class="td-content product-brand text-danger">Sunglasses</div>
                                </td>
                                <td>
                                    <div class="td-content product-invoice">#66894</div>
                                </td>
                                <td>
                                    <div class="td-content pricing"><span class="">$126.04</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="badge badge-success">Paid</span></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="td-content customer-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="avatar"><span>Ryan Collins</span></div>
                                </td>
                                <td>
                                    <div class="td-content product-brand text-warning">Sport</div>
                                </td>
                                <td>
                                    <div class="td-content product-invoice">#89891</div>
                                </td>
                                <td>
                                    <div class="td-content pricing"><span class="">$108.09</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="badge badge-primary">Shipped</span></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="td-content customer-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="avatar"><span>Irene Collins</span></div>
                                </td>
                                <td>
                                    <div class="td-content product-brand text-primary">Speakers</div>
                                </td>
                                <td>
                                    <div class="td-content product-invoice">#75844</div>
                                </td>
                                <td>
                                    <div class="td-content pricing"><span class="">$84.00</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="badge badge-danger">Pending</span></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="td-content customer-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="avatar"><span>Sonia Shaw</span></div>
                                </td>
                                <td>
                                    <div class="td-content product-brand text-danger">Watch</div>
                                </td>
                                <td>
                                    <div class="td-content product-invoice">#76844</div>
                                </td>
                                <td>
                                    <div class="td-content pricing"><span class="">$110.00</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="badge badge-success">Paid</span></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-table-three">

                <div class="widget-heading">
                    <h5 class="">Top Selling Product</h5>
                </div>

                <div class="widget-content">
                    <div class="table-responsive">
                        <table class="table table-scroll">
                            <thead>
                            <tr>
                                <th>
                                    <div class="th-content">Product</div>
                                </th>
                                <th>
                                    <div class="th-content th-heading">Price</div>
                                </th>
                                <th>
                                    <div class="th-content th-heading">Discount</div>
                                </th>
                                <th>
                                    <div class="th-content">Sold</div>
                                </th>
                                <th>
                                    <div class="th-content">Source</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="td-content product-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="product">
                                        <div class="align-self-center"><p class="prd-name">Headphone</p>
                                            <p class="prd-follow text-primary">Digital</p></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="pricing">$168.09</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="discount-pricing">$60.09</span></div>
                                </td>
                                <td>
                                    <div class="td-content">170</div>
                                </td>
                                <td>
                                    <div class="td-content"><a href="javascript:void(0);" class="text-danger">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-chevrons-right">
                                                <polyline points="13 17 18 12 13 7"></polyline>
                                                <polyline points="6 17 11 12 6 7"></polyline>
                                            </svg>
                                            Direct</a></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="td-content product-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="product">
                                        <div class="align-self-center"><p class="prd-name">Shoes</p>
                                            <p class="prd-follow text-warning">Faishon</p></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="pricing">$108.09</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="discount-pricing">$47.09</span></div>
                                </td>
                                <td>
                                    <div class="td-content">130</div>
                                </td>
                                <td>
                                    <div class="td-content"><a href="javascript:void(0);" class="text-primary">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-chevrons-right">
                                                <polyline points="13 17 18 12 13 7"></polyline>
                                                <polyline points="6 17 11 12 6 7"></polyline>
                                            </svg>
                                            Google</a></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="td-content product-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="product">
                                        <div class="align-self-center"><p class="prd-name">Watch</p>
                                            <p class="prd-follow text-danger">Accessories</p></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="pricing">$88.00</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="discount-pricing">$20.00</span></div>
                                </td>
                                <td>
                                    <div class="td-content">66</div>
                                </td>
                                <td>
                                    <div class="td-content"><a href="javascript:void(0);" class="text-warning">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-chevrons-right">
                                                <polyline points="13 17 18 12 13 7"></polyline>
                                                <polyline points="6 17 11 12 6 7"></polyline>
                                            </svg>
                                            Ads</a></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="td-content product-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="product">
                                        <div class="align-self-center"><p class="prd-name">Laptop</p>
                                            <p class="prd-follow text-primary">Digital</p></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="pricing">$110.00</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="discount-pricing">$33.00</span></div>
                                </td>
                                <td>
                                    <div class="td-content">35</div>
                                </td>
                                <td>
                                    <div class="td-content"><a href="javascript:void(0);" class="text-info">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-chevrons-right">
                                                <polyline points="13 17 18 12 13 7"></polyline>
                                                <polyline points="6 17 11 12 6 7"></polyline>
                                            </svg>
                                            Email</a></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="td-content product-name"><img
                                            src="{{ asset('public')}}/{{app()->getLocale()}}/assets/img/90x90.jpg"
                                            alt="product">
                                        <div class="align-self-center"><p class="prd-name">Camera</p>
                                            <p class="prd-follow text-primary">Digital</p></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="pricing">$126.04</span></div>
                                </td>
                                <td>
                                    <div class="td-content"><span class="discount-pricing">$26.04</span></div>
                                </td>
                                <td>
                                    <div class="td-content">30</div>
                                </td>
                                <td>
                                    <div class="td-content"><a href="javascript:void(0);" class="text-secondary">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-chevrons-right">
                                                <polyline points="13 17 18 12 13 7"></polyline>
                                                <polyline points="6 17 11 12 6 7"></polyline>
                                            </svg>
                                            Referral</a></div>
                                </td>
                            </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing">


            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                <div class="widget-two">
                    <div class="widget-content">
                        <div class="w-numeric-value">
                            <div class="w-content">
                                <span class="w-value">Daily sales</span>
                                <span class="w-numeric-title">Go to columns for details.</span>
                            </div>
                            <div class="w-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-dollar-sign">
                                    <line x1="12" y1="1" x2="12" y2="23"></line>
                                    <path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="w-chart">
                            <div id="daily-sales" style="min-height: 175px;">
                                <div id="apexchartsp1af5bd3h" class="apexcharts-canvas apexchartsp1af5bd3h light"
                                     style="width: 386px; height: 160px;">
                                    <svg id="SvgjsSvg1241" width="386" height="160" xmlns="http://www.w3.org/2000/svg"
                                         version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg"
                                         xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                         style="background: transparent;">
                                        <g id="SvgjsG1243" class="apexcharts-inner apexcharts-graphical"
                                           transform="translate(0, 40)">
                                            <defs id="SvgjsDefs1242">
                                                <linearGradient id="SvgjsLinearGradient1245" x1="0" y1="0" x2="0"
                                                                y2="1">
                                                    <stop id="SvgjsStop1246" stop-opacity="0.4"
                                                          stop-color="rgba(216,227,240,0.4)" offset="0"></stop>
                                                    <stop id="SvgjsStop1247" stop-opacity="0.5"
                                                          stop-color="rgba(190,209,230,0.5)" offset="1"></stop>
                                                    <stop id="SvgjsStop1248" stop-opacity="0.5"
                                                          stop-color="rgba(190,209,230,0.5)" offset="1"></stop>
                                                </linearGradient>
                                                <clipPath id="gridRectMaskp1af5bd3h">
                                                    <rect id="SvgjsRect1250" width="387" height="121" x="-0.5" y="-0.5"
                                                          rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0"
                                                          stroke="none" stroke-dasharray="0"></rect>
                                                </clipPath>
                                                <clipPath id="gridRectMarkerMaskp1af5bd3h">
                                                    <rect id="SvgjsRect1251" width="388" height="122" x="-1" y="-1"
                                                          rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0"
                                                          stroke="none" stroke-dasharray="0"></rect>
                                                </clipPath>
                                            </defs>
                                            <rect id="SvgjsRect1249" width="13.785714285714286" height="120" x="0" y="0"
                                                  rx="0" ry="0" fill="url(#SvgjsLinearGradient1245)" opacity="1"
                                                  stroke-width="0" stroke-dasharray="3" class="apexcharts-xcrosshairs"
                                                  y2="120" filter="none" fill-opacity="0.9"></rect>
                                            <g id="SvgjsG1272" class="apexcharts-xaxis" transform="translate(0, 0)">
                                                <g id="SvgjsG1273" class="apexcharts-xaxis-texts-g"
                                                   transform="translate(0, -4)"></g>
                                                <line id="SvgjsLine1274" x1="0" y1="121" x2="386" y2="121"
                                                      stroke="#78909c" stroke-dasharray="0" stroke-width="1"></line>
                                            </g>
                                            <g id="SvgjsG1276" class="apexcharts-grid">
                                                <line id="SvgjsLine1278" x1="0" y1="120" x2="386" y2="120"
                                                      stroke="transparent" stroke-dasharray="0"></line>
                                                <line id="SvgjsLine1277" x1="0" y1="1" x2="0" y2="120"
                                                      stroke="transparent" stroke-dasharray="0"></line>
                                            </g>
                                            <g id="SvgjsG1253" class="apexcharts-bar-series apexcharts-plot-series">
                                                <g id="SvgjsG1254" class="apexcharts-series" seriesName="Sales" rel="1"
                                                   data:realIndex="0">
                                                    <path id="apexcharts-bar-area-0"
                                                          d="M 20.67857142857143 120L 20.67857142857143 73.33333333333334L 33.464285714285715 73.33333333333334L 33.464285714285715 120L 20.17857142857143 120"
                                                          fill="rgba(226,160,63,1)" fill-opacity="1" stroke="#e2a03f"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 20.67857142857143 120L 20.67857142857143 73.33333333333334L 33.464285714285715 73.33333333333334L 33.464285714285715 120L 20.17857142857143 120"
                                                          pathFrom="M 20.67857142857143 120L 20.67857142857143 120L 33.464285714285715 120L 33.464285714285715 120L 33.464285714285715 120L 20.17857142857143 120"
                                                          cy="73.33333333333334" cx="75.32142857142858" j="0"
                                                          val="38.888888888888886" barHeight="46.666666666666664"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-0"
                                                          d="M 75.82142857142858 120L 75.82142857142858 46.28571428571429L 88.60714285714288 46.28571428571429L 88.60714285714288 120L 75.32142857142858 120"
                                                          fill="rgba(226,160,63,1)" fill-opacity="1" stroke="#e2a03f"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 75.82142857142858 120L 75.82142857142858 46.28571428571429L 88.60714285714288 46.28571428571429L 88.60714285714288 120L 75.32142857142858 120"
                                                          pathFrom="M 75.82142857142858 120L 75.82142857142858 120L 88.60714285714288 120L 88.60714285714288 120L 88.60714285714288 120L 75.32142857142858 120"
                                                          cy="46.28571428571429" cx="130.46428571428572" j="1"
                                                          val="61.42857142857143" barHeight="73.71428571428571"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-0"
                                                          d="M 130.96428571428572 120L 130.96428571428572 44.571428571428584L 143.75 44.571428571428584L 143.75 120L 130.46428571428572 120"
                                                          fill="rgba(226,160,63,1)" fill-opacity="1" stroke="#e2a03f"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 130.96428571428572 120L 130.96428571428572 44.571428571428584L 143.75 44.571428571428584L 143.75 120L 130.46428571428572 120"
                                                          pathFrom="M 130.96428571428572 120L 130.96428571428572 120L 143.75 120L 143.75 120L 143.75 120L 130.46428571428572 120"
                                                          cy="44.571428571428584" cx="185.60714285714286" j="2"
                                                          val="62.857142857142854" barHeight="75.42857142857142"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-0"
                                                          d="M 186.10714285714286 120L 186.10714285714286 12.800000000000011L 198.89285714285714 12.800000000000011L 198.89285714285714 120L 185.60714285714286 120"
                                                          fill="rgba(226,160,63,1)" fill-opacity="1" stroke="#e2a03f"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 186.10714285714286 120L 186.10714285714286 12.800000000000011L 198.89285714285714 12.800000000000011L 198.89285714285714 120L 185.60714285714286 120"
                                                          pathFrom="M 186.10714285714286 120L 186.10714285714286 120L 198.89285714285714 120L 198.89285714285714 120L 198.89285714285714 120L 185.60714285714286 120"
                                                          cy="12.800000000000011" cx="240.75" j="3"
                                                          val="89.33333333333333" barHeight="107.19999999999999"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-0"
                                                          d="M 241.25 120L 241.25 39.344262295081975L 254.03571428571428 39.344262295081975L 254.03571428571428 120L 240.75 120"
                                                          fill="rgba(226,160,63,1)" fill-opacity="1" stroke="#e2a03f"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 241.25 120L 241.25 39.344262295081975L 254.03571428571428 39.344262295081975L 254.03571428571428 120L 240.75 120"
                                                          pathFrom="M 241.25 120L 241.25 120L 254.03571428571428 120L 254.03571428571428 120L 254.03571428571428 120L 240.75 120"
                                                          cy="39.344262295081975" cx="295.89285714285717" j="4"
                                                          val="67.21311475409836" barHeight="80.65573770491802"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-0"
                                                          d="M 296.39285714285717 120L 296.39285714285717 35.38461538461539L 309.17857142857144 35.38461538461539L 309.17857142857144 120L 295.89285714285717 120"
                                                          fill="rgba(226,160,63,1)" fill-opacity="1" stroke="#e2a03f"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 296.39285714285717 120L 296.39285714285717 35.38461538461539L 309.17857142857144 35.38461538461539L 309.17857142857144 120L 295.89285714285717 120"
                                                          pathFrom="M 296.39285714285717 120L 296.39285714285717 120L 309.17857142857144 120L 309.17857142857144 120L 309.17857142857144 120L 295.89285714285717 120"
                                                          cy="35.38461538461539" cx="351.03571428571433" j="5"
                                                          val="70.51282051282051" barHeight="84.61538461538461"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-0"
                                                          d="M 351.53571428571433 120L 351.53571428571433 27.368421052631575L 364.3214285714286 27.368421052631575L 364.3214285714286 120L 351.03571428571433 120"
                                                          fill="rgba(226,160,63,1)" fill-opacity="1" stroke="#e2a03f"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 351.53571428571433 120L 351.53571428571433 27.368421052631575L 364.3214285714286 27.368421052631575L 364.3214285714286 120L 351.03571428571433 120"
                                                          pathFrom="M 351.53571428571433 120L 351.53571428571433 120L 364.3214285714286 120L 364.3214285714286 120L 364.3214285714286 120L 351.03571428571433 120"
                                                          cy="27.368421052631575" cx="406.1785714285715" j="6"
                                                          val="77.19298245614036" barHeight="92.63157894736842"
                                                          barWidth="13.785714285714286"></path>
                                                    <g id="SvgjsG1255" class="apexcharts-datalabels"></g>
                                                </g>
                                                <g id="SvgjsG1263" class="apexcharts-series" seriesName="LastxWeek"
                                                   rel="2" data:realIndex="1">
                                                    <path id="apexcharts-bar-area-1"
                                                          d="M 20.67857142857143 73.33333333333334L 20.67857142857143 1.4210854715202004e-14L 33.464285714285715 1.4210854715202004e-14L 33.464285714285715 73.33333333333334L 20.17857142857143 73.33333333333334"
                                                          fill="rgba(224,230,237,1)" fill-opacity="1" stroke="#e0e6ed"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 20.67857142857143 73.33333333333334L 20.67857142857143 1.4210854715202004e-14L 33.464285714285715 1.4210854715202004e-14L 33.464285714285715 73.33333333333334L 20.17857142857143 73.33333333333334"
                                                          pathFrom="M 20.67857142857143 73.33333333333334L 20.67857142857143 73.33333333333334L 33.464285714285715 73.33333333333334L 33.464285714285715 73.33333333333334L 33.464285714285715 73.33333333333334L 20.17857142857143 73.33333333333334"
                                                          cy="1.4210854715202004e-14" cx="75.32142857142858" j="0"
                                                          val="61.111111111111114" barHeight="73.33333333333333"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-1"
                                                          d="M 75.82142857142858 46.28571428571429L 75.82142857142858 7.105427357601002e-15L 88.60714285714288 7.105427357601002e-15L 88.60714285714288 46.28571428571429L 75.32142857142858 46.28571428571429"
                                                          fill="rgba(224,230,237,1)" fill-opacity="1" stroke="#e0e6ed"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 75.82142857142858 46.28571428571429L 75.82142857142858 7.105427357601002e-15L 88.60714285714288 7.105427357601002e-15L 88.60714285714288 46.28571428571429L 75.32142857142858 46.28571428571429"
                                                          pathFrom="M 75.82142857142858 46.28571428571429L 75.82142857142858 46.28571428571429L 88.60714285714288 46.28571428571429L 88.60714285714288 46.28571428571429L 88.60714285714288 46.28571428571429L 75.32142857142858 46.28571428571429"
                                                          cy="7.105427357601002e-15" cx="130.46428571428572" j="1"
                                                          val="38.57142857142857" barHeight="46.285714285714285"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-1"
                                                          d="M 130.96428571428572 44.571428571428584L 130.96428571428572 7.105427357601002e-15L 143.75 7.105427357601002e-15L 143.75 44.571428571428584L 130.46428571428572 44.571428571428584"
                                                          fill="rgba(224,230,237,1)" fill-opacity="1" stroke="#e0e6ed"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 130.96428571428572 44.571428571428584L 130.96428571428572 7.105427357601002e-15L 143.75 7.105427357601002e-15L 143.75 44.571428571428584L 130.46428571428572 44.571428571428584"
                                                          pathFrom="M 130.96428571428572 44.571428571428584L 130.96428571428572 44.571428571428584L 143.75 44.571428571428584L 143.75 44.571428571428584L 143.75 44.571428571428584L 130.46428571428572 44.571428571428584"
                                                          cy="7.105427357601002e-15" cx="185.60714285714286" j="2"
                                                          val="37.142857142857146" barHeight="44.57142857142858"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-1"
                                                          d="M 186.10714285714286 12.800000000000011L 186.10714285714286 1.2434497875801753e-14L 198.89285714285714 1.2434497875801753e-14L 198.89285714285714 12.800000000000011L 185.60714285714286 12.800000000000011"
                                                          fill="rgba(224,230,237,1)" fill-opacity="1" stroke="#e0e6ed"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 186.10714285714286 12.800000000000011L 186.10714285714286 1.2434497875801753e-14L 198.89285714285714 1.2434497875801753e-14L 198.89285714285714 12.800000000000011L 185.60714285714286 12.800000000000011"
                                                          pathFrom="M 186.10714285714286 12.800000000000011L 186.10714285714286 12.800000000000011L 198.89285714285714 12.800000000000011L 198.89285714285714 12.800000000000011L 198.89285714285714 12.800000000000011L 185.60714285714286 12.800000000000011"
                                                          cy="1.2434497875801753e-14" cx="240.75" j="3"
                                                          val="10.666666666666666" barHeight="12.799999999999999"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-1"
                                                          d="M 241.25 39.344262295081975L 241.25 1.4210854715202004e-14L 254.03571428571428 1.4210854715202004e-14L 254.03571428571428 39.344262295081975L 240.75 39.344262295081975"
                                                          fill="rgba(224,230,237,1)" fill-opacity="1" stroke="#e0e6ed"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 241.25 39.344262295081975L 241.25 1.4210854715202004e-14L 254.03571428571428 1.4210854715202004e-14L 254.03571428571428 39.344262295081975L 240.75 39.344262295081975"
                                                          pathFrom="M 241.25 39.344262295081975L 241.25 39.344262295081975L 254.03571428571428 39.344262295081975L 254.03571428571428 39.344262295081975L 254.03571428571428 39.344262295081975L 240.75 39.344262295081975"
                                                          cy="1.4210854715202004e-14" cx="295.89285714285717" j="4"
                                                          val="32.78688524590164" barHeight="39.34426229508196"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-1"
                                                          d="M 296.39285714285717 35.38461538461539L 296.39285714285717 7.105427357601002e-15L 309.17857142857144 7.105427357601002e-15L 309.17857142857144 35.38461538461539L 295.89285714285717 35.38461538461539"
                                                          fill="rgba(224,230,237,1)" fill-opacity="1" stroke="#e0e6ed"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 296.39285714285717 35.38461538461539L 296.39285714285717 7.105427357601002e-15L 309.17857142857144 7.105427357601002e-15L 309.17857142857144 35.38461538461539L 295.89285714285717 35.38461538461539"
                                                          pathFrom="M 296.39285714285717 35.38461538461539L 296.39285714285717 35.38461538461539L 309.17857142857144 35.38461538461539L 309.17857142857144 35.38461538461539L 309.17857142857144 35.38461538461539L 295.89285714285717 35.38461538461539"
                                                          cy="7.105427357601002e-15" cx="351.03571428571433" j="5"
                                                          val="29.487179487179485" barHeight="35.38461538461538"
                                                          barWidth="13.785714285714286"></path>
                                                    <path id="apexcharts-bar-area-1"
                                                          d="M 351.53571428571433 27.368421052631575L 351.53571428571433 -3.552713678800501e-15L 364.3214285714286 -3.552713678800501e-15L 364.3214285714286 27.368421052631575L 351.03571428571433 27.368421052631575"
                                                          fill="rgba(224,230,237,1)" fill-opacity="1" stroke="#e0e6ed"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="1"
                                                          stroke-dasharray="0" class="apexcharts-bar-area" index="1"
                                                          clip-path="url(#gridRectMaskp1af5bd3h)"
                                                          pathTo="M 351.53571428571433 27.368421052631575L 351.53571428571433 -3.552713678800501e-15L 364.3214285714286 -3.552713678800501e-15L 364.3214285714286 27.368421052631575L 351.03571428571433 27.368421052631575"
                                                          pathFrom="M 351.53571428571433 27.368421052631575L 351.53571428571433 27.368421052631575L 364.3214285714286 27.368421052631575L 364.3214285714286 27.368421052631575L 364.3214285714286 27.368421052631575L 351.03571428571433 27.368421052631575"
                                                          cy="-3.552713678800501e-15" cx="406.1785714285715" j="6"
                                                          val="22.80701754385965" barHeight="27.36842105263158"
                                                          barWidth="13.785714285714286"></path>
                                                    <g id="SvgjsG1264" class="apexcharts-datalabels"></g>
                                                </g>
                                            </g>
                                            <line id="SvgjsLine1279" x1="0" y1="0" x2="386" y2="0" stroke="#b6b6b6"
                                                  stroke-dasharray="0" stroke-width="1"
                                                  class="apexcharts-ycrosshairs"></line>
                                            <line id="SvgjsLine1280" x1="0" y1="0" x2="386" y2="0" stroke-dasharray="0"
                                                  stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line>
                                            <g id="SvgjsG1281" class="apexcharts-yaxis-annotations"></g>
                                            <g id="SvgjsG1282" class="apexcharts-xaxis-annotations"></g>
                                            <g id="SvgjsG1283" class="apexcharts-point-annotations"></g>
                                        </g>
                                        <g id="SvgjsG1275" class="apexcharts-yaxis" rel="0"
                                           transform="translate(-21, 0)"></g>
                                    </svg>
                                    <div class="apexcharts-legend"></div>
                                    <div class="apexcharts-tooltip light">
                                        <div class="apexcharts-tooltip-title"
                                             style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"></div>
                                        <div class="apexcharts-tooltip-series-group"><span
                                                class="apexcharts-tooltip-marker"
                                                style="background-color: rgb(226, 160, 63);"></span>
                                            <div class="apexcharts-tooltip-text"
                                                 style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-label"></span><span
                                                        class="apexcharts-tooltip-text-value"></span></div>
                                                <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span></div>
                                            </div>
                                        </div>
                                        <div class="apexcharts-tooltip-series-group"><span
                                                class="apexcharts-tooltip-marker"
                                                style="background-color: rgb(224, 230, 237);"></span>
                                            <div class="apexcharts-tooltip-text"
                                                 style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-label"></span><span
                                                        class="apexcharts-tooltip-text-value"></span></div>
                                                <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="resize-triggers">
                                <div class="expand-trigger">
                                    <div style="width: 387px; height: 176px;"></div>
                                </div>
                                <div class="contract-trigger"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                <div class="widget widget-three">
                    <div class="widget-heading">
                        <h5 class="">Summary</h5>

                        <div class="task-action">
                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" role="button" id="pendingTask"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-more-horizontal">
                                        <circle cx="12" cy="12" r="1"></circle>
                                        <circle cx="19" cy="12" r="1"></circle>
                                        <circle cx="5" cy="12" r="1"></circle>
                                    </svg>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="pendingTask"
                                     style="will-change: transform;">
                                    <a class="dropdown-item" href="javascript:void(0);">View Report</a>
                                    <a class="dropdown-item" href="javascript:void(0);">Edit Report</a>
                                    <a class="dropdown-item" href="javascript:void(0);">Mark as Done</a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="widget-content">

                        <div class="order-summary">

                            <div class="summary-list">
                                <div class="w-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-shopping-bag">
                                        <path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path>
                                        <line x1="3" y1="6" x2="21" y2="6"></line>
                                        <path d="M16 10a4 4 0 0 1-8 0"></path>
                                    </svg>
                                </div>
                                <div class="w-summary-details">

                                    <div class="w-summary-info">
                                        <h6>Income</h6>
                                        <p class="summary-count">$92,600</p>
                                    </div>

                                    <div class="w-summary-stats">
                                        <div class="progress">
                                            <div class="progress-bar bg-gradient-secondary" role="progressbar"
                                                 style="width: 90%" aria-valuenow="90" aria-valuemin="0"
                                                 aria-valuemax="100"></div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="summary-list">
                                <div class="w-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-tag">
                                        <path
                                            d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path>
                                        <line x1="7" y1="7" x2="7" y2="7"></line>
                                    </svg>
                                </div>
                                <div class="w-summary-details">

                                    <div class="w-summary-info">
                                        <h6>Profit</h6>
                                        <p class="summary-count">$37,515</p>
                                    </div>

                                    <div class="w-summary-stats">
                                        <div class="progress">
                                            <div class="progress-bar bg-gradient-success" role="progressbar"
                                                 style="width: 65%" aria-valuenow="65" aria-valuemin="0"
                                                 aria-valuemax="100"></div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="summary-list">
                                <div class="w-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-credit-card">
                                        <rect x="1" y="4" width="22" height="16" rx="2" ry="2"></rect>
                                        <line x1="1" y1="10" x2="23" y2="10"></line>
                                    </svg>
                                </div>
                                <div class="w-summary-details">

                                    <div class="w-summary-info">
                                        <h6>Expenses</h6>
                                        <p class="summary-count">$55,085</p>
                                    </div>

                                    <div class="w-summary-stats">
                                        <div class="progress">
                                            <div class="progress-bar bg-gradient-warning" role="progressbar"
                                                 style="width: 80%" aria-valuenow="80" aria-valuemin="0"
                                                 aria-valuemax="100"></div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                <div class="widget-one widget">
                    <div class="widget-content">
                        <div class="w-numeric-value">
                            <div class="w-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-shopping-cart">
                                    <circle cx="9" cy="21" r="1"></circle>
                                    <circle cx="20" cy="21" r="1"></circle>
                                    <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
                                </svg>
                            </div>
                            <div class="w-content">
                                <span class="w-value">3,192</span>
                                <span class="w-numeric-title">Total Orders</span>
                            </div>
                        </div>
                        <div class="w-chart" style="position: relative;">
                            <div id="total-orders" style="min-height: 290px;">
                                <div id="apexchartsgi2gld2k" class="apexcharts-canvas apexchartsgi2gld2k light"
                                     style="width: 386px; height: 290px;">
                                    <svg id="SvgjsSvg1287" width="386" height="290" xmlns="http://www.w3.org/2000/svg"
                                         version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg"
                                         xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                         style="background: transparent;">
                                        <g id="SvgjsG1289" class="apexcharts-inner apexcharts-graphical"
                                           transform="translate(0, 125)">
                                            <defs id="SvgjsDefs1288">
                                                <clipPath id="gridRectMaskgi2gld2k">
                                                    <rect id="SvgjsRect1293" width="388" height="167" x="-1" y="-1"
                                                          rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0"
                                                          stroke="none" stroke-dasharray="0"></rect>
                                                </clipPath>
                                                <clipPath id="gridRectMarkerMaskgi2gld2k">
                                                    <rect id="SvgjsRect1294" width="388" height="167" x="-1" y="-1"
                                                          rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0"
                                                          stroke="none" stroke-dasharray="0"></rect>
                                                </clipPath>
                                                <linearGradient id="SvgjsLinearGradient1300" x1="0" y1="0" x2="0"
                                                                y2="1">
                                                    <stop id="SvgjsStop1301" stop-opacity="0.65"
                                                          stop-color="rgba(26,188,156,0.65)" offset="0"></stop>
                                                    <stop id="SvgjsStop1302" stop-opacity="0.5"
                                                          stop-color="rgba(141,222,206,0.5)" offset="1"></stop>
                                                    <stop id="SvgjsStop1303" stop-opacity="0.5"
                                                          stop-color="rgba(141,222,206,0.5)" offset="1"></stop>
                                                </linearGradient>
                                            </defs>
                                            <line id="SvgjsLine1292" x1="0" y1="0" x2="0" y2="165" stroke="#b6b6b6"
                                                  stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0"
                                                  width="1" height="165" fill="#b1b9c4" filter="none" fill-opacity="0.9"
                                                  stroke-width="1"></line>
                                            <g id="SvgjsG1306" class="apexcharts-xaxis" transform="translate(0, 0)">
                                                <g id="SvgjsG1307" class="apexcharts-xaxis-texts-g"
                                                   transform="translate(0, -4)"></g>
                                            </g>
                                            <g id="SvgjsG1310" class="apexcharts-grid">
                                                <line id="SvgjsLine1312" x1="0" y1="165" x2="386" y2="165"
                                                      stroke="transparent" stroke-dasharray="0"></line>
                                                <line id="SvgjsLine1311" x1="0" y1="1" x2="0" y2="165"
                                                      stroke="transparent" stroke-dasharray="0"></line>
                                            </g>
                                            <g id="SvgjsG1296" class="apexcharts-area-series apexcharts-plot-series">
                                                <g id="SvgjsG1297" class="apexcharts-series" seriesName="Sales"
                                                   data:longestSeries="true" rel="1" data:realIndex="0">
                                                    <path id="apexcharts-area-0"
                                                          d="M 0 165L 0 57.85714285714285C 15.011111111111111 57.85714285714285 27.87777777777778 68.57142857142856 42.88888888888889 68.57142857142856C 57.900000000000006 68.57142857142856 70.76666666666668 25.714285714285694 85.77777777777779 25.714285714285694C 100.78888888888889 25.714285714285694 113.65555555555555 63.21428571428571 128.66666666666666 63.21428571428571C 143.67777777777778 63.21428571428571 156.54444444444445 4.285714285714278 171.55555555555557 4.285714285714278C 186.5666666666667 4.285714285714278 199.43333333333334 63.21428571428571 214.44444444444446 63.21428571428571C 229.45555555555555 63.21428571428571 242.32222222222222 25.714285714285694 257.3333333333333 25.714285714285694C 272.34444444444443 25.714285714285694 285.2111111111111 68.57142857142856 300.22222222222223 68.57142857142856C 315.23333333333335 68.57142857142856 328.1 57.85714285714285 343.11111111111114 57.85714285714285C 358.12222222222226 57.85714285714285 370.9888888888889 89.99999999999999 386 89.99999999999999C 386 89.99999999999999 386 89.99999999999999 386 165M 386 89.99999999999999z"
                                                          fill="url(#SvgjsLinearGradient1300)" fill-opacity="1"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="0"
                                                          stroke-dasharray="0" class="apexcharts-area" index="0"
                                                          clip-path="url(#gridRectMaskgi2gld2k)"
                                                          pathTo="M 0 165L 0 57.85714285714285C 15.011111111111111 57.85714285714285 27.87777777777778 68.57142857142856 42.88888888888889 68.57142857142856C 57.900000000000006 68.57142857142856 70.76666666666668 25.714285714285694 85.77777777777779 25.714285714285694C 100.78888888888889 25.714285714285694 113.65555555555555 63.21428571428571 128.66666666666666 63.21428571428571C 143.67777777777778 63.21428571428571 156.54444444444445 4.285714285714278 171.55555555555557 4.285714285714278C 186.5666666666667 4.285714285714278 199.43333333333334 63.21428571428571 214.44444444444446 63.21428571428571C 229.45555555555555 63.21428571428571 242.32222222222222 25.714285714285694 257.3333333333333 25.714285714285694C 272.34444444444443 25.714285714285694 285.2111111111111 68.57142857142856 300.22222222222223 68.57142857142856C 315.23333333333335 68.57142857142856 328.1 57.85714285714285 343.11111111111114 57.85714285714285C 358.12222222222226 57.85714285714285 370.9888888888889 89.99999999999999 386 89.99999999999999C 386 89.99999999999999 386 89.99999999999999 386 165M 386 89.99999999999999z"
                                                          pathFrom="M -1 165L -1 165L 42.88888888888889 165L 85.77777777777779 165L 128.66666666666666 165L 171.55555555555557 165L 214.44444444444446 165L 257.3333333333333 165L 300.22222222222223 165L 343.11111111111114 165L 386 165"></path>
                                                    <path id="apexcharts-area-0"
                                                          d="M 0 57.85714285714285C 15.011111111111111 57.85714285714285 27.87777777777778 68.57142857142856 42.88888888888889 68.57142857142856C 57.900000000000006 68.57142857142856 70.76666666666668 25.714285714285694 85.77777777777779 25.714285714285694C 100.78888888888889 25.714285714285694 113.65555555555555 63.21428571428571 128.66666666666666 63.21428571428571C 143.67777777777778 63.21428571428571 156.54444444444445 4.285714285714278 171.55555555555557 4.285714285714278C 186.5666666666667 4.285714285714278 199.43333333333334 63.21428571428571 214.44444444444446 63.21428571428571C 229.45555555555555 63.21428571428571 242.32222222222222 25.714285714285694 257.3333333333333 25.714285714285694C 272.34444444444443 25.714285714285694 285.2111111111111 68.57142857142856 300.22222222222223 68.57142857142856C 315.23333333333335 68.57142857142856 328.1 57.85714285714285 343.11111111111114 57.85714285714285C 358.12222222222226 57.85714285714285 370.9888888888889 89.99999999999999 386 89.99999999999999"
                                                          fill="none" fill-opacity="1" stroke="#1abc9c"
                                                          stroke-opacity="1" stroke-linecap="butt" stroke-width="2"
                                                          stroke-dasharray="0" class="apexcharts-area" index="0"
                                                          clip-path="url(#gridRectMaskgi2gld2k)"
                                                          pathTo="M 0 57.85714285714285C 15.011111111111111 57.85714285714285 27.87777777777778 68.57142857142856 42.88888888888889 68.57142857142856C 57.900000000000006 68.57142857142856 70.76666666666668 25.714285714285694 85.77777777777779 25.714285714285694C 100.78888888888889 25.714285714285694 113.65555555555555 63.21428571428571 128.66666666666666 63.21428571428571C 143.67777777777778 63.21428571428571 156.54444444444445 4.285714285714278 171.55555555555557 4.285714285714278C 186.5666666666667 4.285714285714278 199.43333333333334 63.21428571428571 214.44444444444446 63.21428571428571C 229.45555555555555 63.21428571428571 242.32222222222222 25.714285714285694 257.3333333333333 25.714285714285694C 272.34444444444443 25.714285714285694 285.2111111111111 68.57142857142856 300.22222222222223 68.57142857142856C 315.23333333333335 68.57142857142856 328.1 57.85714285714285 343.11111111111114 57.85714285714285C 358.12222222222226 57.85714285714285 370.9888888888889 89.99999999999999 386 89.99999999999999"
                                                          pathFrom="M -1 165L -1 165L 42.88888888888889 165L 85.77777777777779 165L 128.66666666666666 165L 171.55555555555557 165L 214.44444444444446 165L 257.3333333333333 165L 300.22222222222223 165L 343.11111111111114 165L 386 165"></path>
                                                    <g id="SvgjsG1298" class="apexcharts-series-markers-wrap">
                                                        <g class="apexcharts-series-markers">
                                                            <circle id="SvgjsCircle1318" r="0" cx="0" cy="0"
                                                                    class="apexcharts-marker wa2haxkm7 no-pointer-events"
                                                                    stroke="#ffffff" fill="#1abc9c" fill-opacity="1"
                                                                    stroke-width="2" stroke-opacity="0.9"
                                                                    default-marker-size="0"></circle>
                                                        </g>
                                                    </g>
                                                    <g id="SvgjsG1299" class="apexcharts-datalabels"></g>
                                                </g>
                                            </g>
                                            <line id="SvgjsLine1313" x1="0" y1="0" x2="386" y2="0" stroke="#b6b6b6"
                                                  stroke-dasharray="0" stroke-width="1"
                                                  class="apexcharts-ycrosshairs"></line>
                                            <line id="SvgjsLine1314" x1="0" y1="0" x2="386" y2="0" stroke-dasharray="0"
                                                  stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line>
                                            <g id="SvgjsG1315" class="apexcharts-yaxis-annotations"></g>
                                            <g id="SvgjsG1316" class="apexcharts-xaxis-annotations"></g>
                                            <g id="SvgjsG1317" class="apexcharts-point-annotations"></g>
                                        </g>
                                        <rect id="SvgjsRect1291" width="0" height="0" x="0" y="0" rx="0" ry="0"
                                              fill="#fefefe" opacity="1" stroke-width="0" stroke="none"
                                              stroke-dasharray="0"></rect>
                                        <g id="SvgjsG1308" class="apexcharts-yaxis" rel="0"
                                           transform="translate(-21, 0)">
                                            <g id="SvgjsG1309" class="apexcharts-yaxis-texts-g"></g>
                                        </g>
                                    </svg>
                                    <div class="apexcharts-legend"></div>
                                    <div class="apexcharts-tooltip dark">
                                        <div class="apexcharts-tooltip-series-group"><span
                                                class="apexcharts-tooltip-marker"
                                                style="background-color: rgb(26, 188, 156);"></span>
                                            <div class="apexcharts-tooltip-text"
                                                 style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-label"></span><span
                                                        class="apexcharts-tooltip-text-value"></span></div>
                                                <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="resize-triggers">
                                <div class="expand-trigger">
                                    <div style="width: 387px; height: 291px;"></div>
                                </div>
                                <div class="contract-trigger"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">

                <div class="widget widget-activity-four">

                    <div class="widget-heading">
                        <h5 class="">@lang('site.country')</h5>
                    </div>

                    <div class="widget-content">

                        <div class="mt-container mx-auto ps ps--active-y">
                            <div class="timeline-line">

                                @foreach(\App\Models\Country::paginate(10) as $country)
                                    <div class="item-timeline timeline-success">
                                        <div class="t-dot" data-original-title="" title="">
                                        </div>
                                        <div class="t-text">
                                            <p>{{$country->name_ar}} <a href="javascript:void(0);"></a>  <a
                                                    href="javascript:void(0);"></a></p>
                                            <p>{{$country->name_en}} <a href="javascript:void(0);"></a>  <a
                                                    href="javascript:void(0);"></a></p>
                                            <span class="badge">{{ $country->status==1?__('site.active'):__('site.inActive') }}</span>
                                            <p class="t-time">{{$country->created_at->toDateString()}}</p>
                                        </div>
                                    </div>
                                @endforeach


                                <div class="item-timeline  timeline-warning">
                                    <div class="t-dot" data-original-title="" title="">
                                    </div>
                                    <div class="t-text">
                                        <p>Conference call with <a href="javascript:void(0);">Marketing Manager</a>.</p>
                                        <span class="badge">In progress</span>
                                        <p class="t-time">17:00</p>
                                    </div>
                                </div>

                            </div>
                            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                            </div>
                            <div class="ps__rail-y" style="top: 0px; height: 326px; right: 353px;">
                                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 169px;"></div>
                            </div>
                        </div>

                        <div class="tm-action-btn">
                            <button class="btn"><span>View All</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-arrow-right">
                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                    <polyline points="12 5 19 12 12 19"></polyline>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
              <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">

                <div class="widget widget-activity-four">

                    <div class="widget-heading">
                        <h5 class="">@lang('site.sponsor')</h5>
                    </div>

                    <div class="widget-content">

                        <div class="mt-container mx-auto ps ps--active-y">
                            <div class="timeline-line">

                                @foreach(\App\Models\Sponsor::paginate(10) as $country)
                                    <div class="item-timeline timeline-success">
                                        <div class="t-dot" data-original-title="" title="">
                                        </div>
                                        <div class="t-text">
                                            <p>{{$country->name_ar}} <a href="javascript:void(0);"></a>  <a
                                                    href="javascript:void(0);"></a></p>
                                            <p>{{$country->name_en}} <a href="javascript:void(0);"></a>  <a
                                                    href="javascript:void(0);"></a></p>
                                            <span class="badge">{{ $country->status==1?__('site.active'):__('site.inActive') }}</span>
                                            <p class="t-time">{{$country->created_at->toDateString()}}</p>
                                        </div>
                                    </div>
                                @endforeach


                                <div class="item-timeline  timeline-warning">
                                    <div class="t-dot" data-original-title="" title="">
                                    </div>
                                    <div class="t-text">
                                        <p>Conference call with <a href="javascript:void(0);">Marketing Manager</a>.</p>
                                        <span class="badge">In progress</span>
                                        <p class="t-time">17:00</p>
                                    </div>
                                </div>

                            </div>
                            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                            </div>
                            <div class="ps__rail-y" style="top: 0px; height: 326px; right: 353px;">
                                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 169px;"></div>
                            </div>
                        </div>

                        <div class="tm-action-btn">
                            <button class="btn"><span>View All</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-arrow-right">
                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                    <polyline points="12 5 19 12 12 19"></polyline>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
              <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">

                <div class="widget widget-activity-four">

                    <div class="widget-heading">
                        <h5 class="">@lang('site.camps')</h5>
                    </div>

                    <div class="widget-content">

                        <div class="mt-container mx-auto ps ps--active-y">
                            <div class="timeline-line">

                                @foreach(\App\Models\Camp::paginate(10) as $country)
                                    <div class="item-timeline timeline-success">
                                        <div class="t-dot" data-original-title="" title="">
                                        </div>
                                        <div class="t-text">
                                            <p>{{$country->name_ar}} <a href="javascript:void(0);"></a>  <a
                                                    href="javascript:void(0);"></a></p>
                                            <p>{{$country->name_en}} <a href="javascript:void(0);"></a>  <a
                                                    href="javascript:void(0);"></a></p>
                                            <span class="badge">{{ $country->status==1?__('site.active'):__('site.inActive') }}</span>
                                            <p class="t-time">{{$country->created_at->toDateString()}}</p>
                                        </div>
                                    </div>
                                @endforeach


                                <div class="item-timeline  timeline-warning">
                                    <div class="t-dot" data-original-title="" title="">
                                    </div>
                                    <div class="t-text">
                                        <p>Conference call with <a href="javascript:void(0);">Marketing Manager</a>.</p>
                                        <span class="badge">In progress</span>
                                        <p class="t-time">17:00</p>
                                    </div>
                                </div>

                            </div>
                            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                            </div>
                            <div class="ps__rail-y" style="top: 0px; height: 326px; right: 353px;">
                                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 169px;"></div>
                            </div>
                        </div>

                        <div class="tm-action-btn">
                            <button class="btn"><span>View All</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-arrow-right">
                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                    <polyline points="12 5 19 12 12 19"></polyline>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>

@endsection
@section('scripts')
    <script>
        var allFollowCount = {
            chart: {
                height: 350,
                type: 'donut',
                toolbar: {
                    show: false,
                },
            },

            series: [{{ $finance->count()}},{{$drugs->count()}},{{$admin->count()}},{{$media->count()}}],
            labels: ['{{__('site.Finance')}}', '{{__('site.Drugs')}}', '{{__('site.Admin')}}', '{{__('site.Media')}}'],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        }

        var allFollowCount = new ApexCharts(
            document.querySelector("#allCountFollow"),
            allFollowCount
        );

        allFollowCount.render();
        var allFollowStatic = {
            chart: {
                height: 350,
                type: 'area',
                toolbar: {
                    show: false,
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            series: [{
                name: "{{__('site.count')}}",
                data: [@foreach ($follows as $data) {{$data->sum.','}}@endforeach]
            }
            ],


            xaxis: {
                categories:
                    [@foreach ($follows as $data)  "{{$data->month}}-{{$data->year}}"{{','}}@endforeach],
            },

        }
        var allFollowStatic = new ApexCharts(
            document.querySelector("#allFollowStatic"),
            allFollowStatic
        );
        allFollowStatic.render();

        var financeFollows = {
            chart: {
                height: 350,
                type: 'bar',
                toolbar: {
                    show: false,
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [{

                name: "{{__('site.count')}}",
                data: [@foreach ($financeFollows as $data) {{$data->sum.','}}@endforeach]

            }],
            xaxis: {
                title: {
                    text: '{{__('site.month')}}'
                },

                categories: [@foreach ($financeFollows as $data)  "{{$data->month}}-{{$data->year}}"{{','}}@endforeach],

            },
            yaxis: {
                title: {
                    text: '$ (thousands)'
                }
            },
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return "$ " + val
                    }
                }
            }
        }
        var financeFollows = new ApexCharts(
            document.querySelector("#financeFollows"),
            financeFollows
        );
        financeFollows.render();

        var drugsFollows = {
            chart: {
                height: 350,
                type: 'bar',
                toolbar: {
                    show: false,
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [{

                name: "{{__('site.count')}}",
                data: [@foreach ($drugsFollows as $data) {{$data->sum.','}}@endforeach]

            }],
            xaxis: {
                title: {
                    text: '{{__('site.month')}}'
                },
                categories: [@foreach ($drugsFollows as $data)  "{{$data->month}}"{{','}}@endforeach],

            },
            yaxis: {
                title: {
                    text: '$ (thousands)'
                }
            },
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return "$ " + val
                    }
                }
            }
        }
        var drugsFollows = new ApexCharts(
            document.querySelector("#drugsFollows"),
            drugsFollows
        );
        drugsFollows.render();

        var adminFollows = {
            chart: {
                height: 350,
                type: 'bar',
                toolbar: {
                    show: false,
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                // colors: ['f1f3f1','transparent']
            },
            series: [{

                name: "{{__('site.count')}}",
                data: [@foreach ($adminFollows as $data) {{$data->sum.','}}@endforeach]

            }],
            grid: {
                row: {
                    colors: ['rgba(247,255,242,0.95)', 'transparent'], // takes an array which will be repeated on columns
                    opacity: 0.5
                },
            },
            xaxis: {
                title: {
                    text: '{{__('site.month')}}'
                },

                categories: [@foreach ($adminFollows as $data)  "{{$data->month}}"{{','}}@endforeach],

            },
            yaxis: {
                title: {
                    text: '$ (thousands)'
                }
            },
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return "$ " + val
                    }
                }
            }
        }
        var adminFollows = new ApexCharts(
            document.querySelector("#adminFollows"),
            adminFollows
        );
        adminFollows.render();
        var mediaFollows = {
            chart: {
                height: 350,
                type: 'bar',
                toolbar: {
                    show: false,
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                // colors: ['f1f3f1','transparent']
            },
            series: [{

                name: "{{__('site.count')}}",
                data: [@foreach ($mediaFollows as $data) {{$data->sum.','}}@endforeach]

            }],
            grid: {
                row: {
                    colors: ['rgba(247,255,242,0.95)', 'transparent'], // takes an array which will be repeated on columns
                    opacity: 0.5
                },
            },
            xaxis: {
                title: {
                    text: '{{__('site.month')}}'
                },
                categories: [@foreach ($mediaFollows as $data)  "{{$data->month}}"{{','}}@endforeach],

            },
            yaxis: {
                title: {
                    text: '$ (thousands)'
                }
            },
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return "$ " + val
                    }
                }
            }
        }
        var mediaFollows = new ApexCharts(
            document.querySelector("#mediaFollows"),
            mediaFollows
        );
        mediaFollows.render();

    </script>
@endsection
