<!--  BEGIN SIDEBAR  -->
<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">
        <div class="profile-info">
            <figure class="user-cover-image"></figure>
            <div class="user-info">
                <img src="{{ $url}}/{{app()->getLocale()}}/assets/img/90x90.jpg" alt="avatar">

            </div>
        </div>
        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu ">
                <a href="{{route('dashboard')}}" aria-expanded="{{Request::is('dashboard') ?'true':'false'}}"
                   class="dropdown-toggle {{Request::is('dashboard*') ?'':'collapse '}}">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                        <span>@lang('site.dashboard')</span>
                    </div>

                </a>
            </li>
            @if (auth()->user()->hasPermission('read_users'))
                <li class="menu ">
                    <a href="{{route('users.index')}}"
                       aria-expanded="{{Request::is('*users*') ?'true':'false'}}"
                       class="dropdown-toggle {{Request::is('users*') ?'':'collapse '}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>@lang('site.users')</span>
                        </div>

                    </a>
                </li>
            @endif


            <li class="menu ">
                    <a href="#follows" data-toggle="collapse"
                       aria-expanded="{{Request::is('*follows*') ?'true':'false'}}"
                       class="dropdown-toggle {{Request::is('follows*') ?'':'collapse '}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>@lang('site.follows')</span>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-chevron-right">
                                <polyline points="9 18 15 12 9 6"></polyline>
                            </svg>
                        </div>
                    </a>
                    <ul class="collapse submenu recent-submenu mini-recent-submenu list-unstyled {{Request::is('follows*') ?'show':' '}}"
                        id="follows"
                        data-parent="#accordionExample">
                        <li class="{{Request::is('follows') ?'active':''}}">
{{--                            <a href="{{route('follows.index')}}"> @lang('site.all') </a>--}}
                            <a href="{{route('follows.index').'?type=FinanceFollowUp'}}"> @lang('site.FinanceFollowUp') </a>
                            <a href="{{route('follows.index').'?type=DrugsFollowUp'}}"> @lang('site.DrugsFollowUp') </a>
                            <a href="{{route('follows.index').'?type=AdminFollowUp'}}"> @lang('site.AdminFollowUp') </a>
                            <a href="{{route('follows.index').'?type=MediaFollowUp'}}"> @lang('site.MediaFollowUp') </a>
                        </li>

                    </ul>
                </li>
            @if (auth()->user()->hasPermission('read_camps'))
                <li class="menu ">
                    <a href="{{route('camps.index')}}"
                       aria-expanded="{{Request::is('*camps*') ?'true':'false'}}"
                       class="dropdown-toggle {{Request::is('camps*') ?'':'collapse '}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>@lang('site.camps')</span>
                        </div>

                    </a>
                </li>
            @endif

            @if (auth()->user()->hasPermission('read_campStatus'))
                <li class="menu ">
                    <a href="{{route('campStatus.index')}}"
                       aria-expanded="{{Request::is('*campStatus*') ?'true':'false'}}"
                       class="dropdown-toggle {{Request::is('campStatus*') ?'':'collapse '}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>@lang('site.campStatus')</span>
                        </div>

                    </a>
                </li>
            @endif
            @if (auth()->user()->hasPermission('read_sponsors'))
                <li class="menu ">
                    <a href="{{route('sponsors.index')}}"
                       aria-expanded="{{Request::is('*sponsors*') ?'true':'false'}}"
                       class="dropdown-toggle {{Request::is('sponsors*') ?'':'collapse '}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>@lang('site.sponsors')</span>
                        </div>

                    </a>
                </li>
            @endif
            @if (auth()->user()->hasPermission('read_sponsorShips'))
                <li class="menu ">
                    <a href="{{route('sponsorShips.index')}}"
                       aria-expanded="{{Request::is('*sponsorShips*') ?'true':'false'}}"
                       class="dropdown-toggle {{Request::is('sponsorShips*') ?'':'collapse '}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>@lang('site.sponsorShips')</span>
                        </div>

                    </a>
                </li>
            @endif
            @if (auth()->user()->hasPermission('read_years'))
                <li class="menu ">
                    <a href="{{route('years.index')}}"
                       aria-expanded="{{Request::is('*years*') ?'true':'false'}}"
                       class="dropdown-toggle {{Request::is('years*') ?'':'collapse '}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>@lang('site.years')</span>
                        </div>

                    </a>
                </li>
            @endif
            @if (auth()->user()->hasPermission('read_countries'))
                <li class="menu ">
                    <a href="{{route('countries.index')}}"
                       aria-expanded="{{Request::is('*countries*') ?'true':'false'}}"
                       class="dropdown-toggle {{Request::is('countries*') ?'':'collapse '}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>@lang('site.countries')</span>
                        </div>

                    </a>
                </li>
            @endif
            @if (auth()->user()->hasPermission('read_plans'))
                <li class="menu ">
                    <a href="{{route('plans.index')}}"
                       aria-expanded="{{Request::is('*plans*') ?'true':'false'}}"
                       class="dropdown-toggle {{Request::is('plans*') ?'':'collapse '}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>@lang('site.plans')</span>
                        </div>

                    </a>
                </li>
            @endif
            @if (auth()->user()->hasPermission('read_teams'))
                <li class="menu ">
                    <a href="{{route('teams.index')}}"
                       aria-expanded="{{Request::is('*teams*') ?'true':'false'}}"
                       class="dropdown-toggle {{Request::is('teams*') ?'':'collapse '}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>@lang('site.teams')</span>
                        </div>

                    </a>
                </li>
            @endif
            <li class="menu ">
                <a href="{{route('follows.index')}}"
                   aria-expanded="{{Request::is('*follows*') ?'true':'false'}}"
                   class="dropdown-toggle {{Request::is('follows*') ?'':'collapse '}}">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                        <span>@lang('site.follows')</span>
                    </div>

                </a>
            </li>

        </ul>
    </nav>

</div>
<!--  END SIDEBAR  -->
