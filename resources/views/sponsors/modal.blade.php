<div class="modal fade" id="ajax-sponsor-model" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="ajaxSponsorModel"></h4>
            </div>
            <div class="modal-body modal-lg">
                <form action="javascript:void(0)" id="addEditSponsorForm" name="addEditSponsorForm"
                      class="form-horizontal needs-validation" method="POST"  novalidate>
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="name" class="control-label">@lang('site.name_en')</label>
                        <input type="text" class="form-control" id="name_en" name="name_en"
                               placeholder="{{__('site.name_en')}}" value="" maxlength="50" aria-describedby="inputGroupPrepend" required>
                        <span class="invalid-feedback">@lang('site.required')</span>
                    </div>

                    <div class="form-group">
                        <label for="name" class="control-label">{{__('site.name_ar')}}</label>
                        <input type="text" class="form-control" id="name_ar" name="name_ar"
                               placeholder="{{__('site.name_en')}}" value="" maxlength="50" aria-describedby="inputGroupPrepend" required>
                        <span class="invalid-feedback">@lang('site.required')</span>
                    </div>

                    <div class="form-group col-sm-3" style="margin-left: -5px;margin-right: -5px">
                        <label>@lang('site.status')</label>
                        <div class="col-sm-6">
                            <label class="switch s-icons s-outline  s-outline-success  mb-8 mr-4">
                                <input type="checkbox" name="status" id="status"  >

                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary" id="btn-save" value="addNewCountry">@lang('site.save_changes')</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

