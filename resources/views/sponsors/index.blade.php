@extends('layouts.app')
@section('title')
    @lang('site.sponsors')
@endsection
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        .table > thead > tr > th {
            font-size: 11px !important;

        }
    </style>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <nav class="breadcrumb-two" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">@lang('site.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a
                                href="{{route('sponsors.index')}}">@lang('site.sponsors')</a></li>
                    </ol>
                </nav>
            </div>
            <div class="col-sm-6">

                <button type="button" id="addNewSponsor" class="btn btn-primary float-right"> @lang('site.add')</button>

            </div>

        </div>
    </div>


    @include('flash::message')

    <div class="clearfix"></div>


    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing layout-top-spacing">
        <div class="widget-content widget-content-area ">
            @include('sponsors.table')
        </div>
    </div>



   @include('sponsors.modal')
@endsection


@section('scripts')
    @include('inc.datatables_js')
    <script type="text/javascript">
        $(document).ready(function ($) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#addNewSponsor').click(function () {
                $('#addEditSponsorForm').trigger("reset");
                $('#ajaxSponsorModel').html("{{__('site.add').' '.__('site.sponsor')}}");
                $('#ajax-sponsor-model').modal('show');
            });

            $('body').on('click', '.edit', function () {

                var id = $(this).data('id');

                // ajax
                $.ajax({
                    type: "POST",
                    url: "{{ route('edit-sponsor') }}",
                    data: {id: id},
                    dataType: 'json',
                    success: function (res) {
                        $('#ajaxSponsorModel').html("{{__('site.edit').' '.__('site.sponsor')}}");
                        $('#ajax-sponsor-model').modal('show');
                        $('#id').val(res.id);
                        $('#name_ar').val(res.name_ar);
                        $('#name_en').val(res.name_en);
                        $("#status").attr("checked", res.status==true?true: false);

                    }
                });

            });


            $('body').on('click', '#btn-save', function (event) {

                var id = $("#id").val();
                var name_ar = $("#name_ar").val();
                var name_en = $("#name_en").val();
                var status = $("#status").val();

                // $("#btn-save").html('Please Wait...');
                // $("#btn-save").attr("disabled", true);

                // ajax
                $.ajax({
                    type: "POST",
                    url: "{{ route('add-update-sponsor') }}",
                    data: {
                        id: id,
                        name_ar: name_ar,
                        name_en: name_en,
                        status: status,
                    },
                    dataType: 'json',
                    success: function (res) {
                        window.location.reload();
                        $("#btn-save").html('Submit');
                        $("#btn-save").attr("disabled", false);
                    }
                });

            });

        });
    </script>
@endsection
