<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $plan->id }}</p>
</div>

<!-- Year Id Field -->
<div class="col-sm-12">
    {!! Form::label('year_id', 'Year Id:') !!}
    <p>{{ $plan->year_id }}</p>
</div>

<!-- Tour Id Field -->
<div class="col-sm-12">
    {!! Form::label('tour_id', 'Tour Id:') !!}
    <p>{{ $plan->tour_id }}</p>
</div>

<!-- Country Id Field -->
<div class="col-sm-12">
    {!! Form::label('country_id', 'Country Id:') !!}
    <p>{{ $plan->country_id }}</p>
</div>

<!-- Camp Id Field -->
<div class="col-sm-12">
    {!! Form::label('camp_id', 'Camp Id:') !!}
    <p>{{ $plan->camp_id }}</p>
</div>

<!-- Team Id Field -->
<div class="col-sm-12">
    {!! Form::label('team_id', 'Team Id:') !!}
    <p>{{ $plan->team_id }}</p>
</div>

<!-- Sponsor Id Field -->
<div class="col-sm-12">
    {!! Form::label('sponsor_id', 'Sponsor Id:') !!}
    <p>{{ $plan->sponsor_id }}</p>
</div>

<!-- Camp Status Id Field -->
<div class="col-sm-12">
    {!! Form::label('camp_status_id', 'Camp Status Id:') !!}
    <p>{{ $plan->camp_status_id }}</p>
</div>

<!-- Sponsor Ship Id Field -->
<div class="col-sm-12">
    {!! Form::label('sponsor_ship_id', 'Sponsor Ship Id:') !!}
    <p>{{ $plan->sponsor_ship_id }}</p>
</div>

<!-- From Field -->
<div class="col-sm-12">
    {!! Form::label('from', 'From:') !!}
    <p>{{ $plan->from }}</p>
</div>

<!-- To Field -->
<div class="col-sm-12">
    {!! Form::label('to', 'To:') !!}
    <p>{{ $plan->to }}</p>
</div>

<!-- Notes Field -->
<div class="col-sm-12">
    {!! Form::label('notes', 'Notes:') !!}
    <p>{{ $plan->notes }}</p>
</div>

<!-- Need Follow Up Field -->
<div class="col-sm-12">
    {!! Form::label('need_follow_up', 'Need Follow Up:') !!}
    <p>{{ $plan->need_follow_up }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $plan->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $plan->updated_at }}</p>
</div>

<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $plan->type }}</p>
</div>

