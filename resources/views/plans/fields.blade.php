<div class="form-group col-md-6">
    <label>@lang('site.year')</label>
    <select data-placeholder="@lang('site.year')" class="form-control basic" name="year_id"
            aria-describedby="inputGroupPrepend" required>
        <option value="">@lang('site.year')</option>

        @foreach(\App\Models\Year::get() as $year)
            <option
                value="{{$year->id}} "{{@$plan->year_id == $year->id||old('year_id') == $year->id? 'selected' : '' }} >{{$year->name}}</option>
        @endforeach

    </select>
    <span class="invalid-feedback">@lang('site.required')</span>

    @error('year_id')<span class="text-danger">{{ $message }}</span>@enderror

</div>
<!-- tour Id Field -->
<div class="form-group col-md-6">
    <label>@lang('site.tour')</label>
    <select data-placeholder="@lang('site.tour')" class="form-control basic" name="tour_id"
            aria-describedby="inputGroupPrepend" required>
        <option value="">@lang('site.tour')</option>

        @foreach(\App\Models\Tour::get() as $tour)
            <option
                value="{{$tour->id}} "{{@$plan->tour_id == $tour->id||old('tour_id') == $tour->id? 'selected' : '' }} >{{$tour->name}}</option>
        @endforeach

    </select>
    <span class="invalid-feedback">@lang('site.required')</span>

    @error('tour_id')<span class="text-danger">{{ $message }}</span>@enderror

</div>

<div class="form-group col-md-6">
    <label>@lang('site.country')</label>
    <select data-placeholder="@lang('site.country')" class="form-control basic" name="country_id"
            aria-describedby="inputGroupPrepend" required>
        <option value="">@lang('site.country')</option>

        @foreach(\App\Models\Country::get() as $country)
            <option
                value="{{$country->id}} "{{@$plan->country_id == $country->id||old('country_id') == $country->id? 'selected' : '' }} >{{$country->name}}</option>
        @endforeach

    </select>
    <span class="invalid-feedback">@lang('site.required')</span>

    @error('country_id')<span class="text-danger">{{ $message }}</span>@enderror

</div>


<div class="form-group col-md-6">
    <label>@lang('site.camp')</label>
    <select data-placeholder="@lang('site.camp')" class="form-control basic" name="camp_id"
            aria-describedby="inputGroupPrepend" required>
        <option value="">@lang('site.camp')</option>

        @foreach(\App\Models\Camp::get() as $camp)
            <option
                value="{{$camp->id}} "{{@$plan->camp_id == $camp->id||old('camp_id') == $camp->id? 'selected' : '' }} >{{$camp->name}}</option>
        @endforeach

    </select>
    <span class="invalid-feedback">@lang('site.required')</span>

    @error('camp_id')<span class="text-danger">{{ $message }}</span>@enderror

</div>
<div class="form-group col-md-6">
    <label>@lang('site.team')</label>
    <select data-placeholder="@lang('site.team')" class="form-control basic" name="team_id"
            aria-describedby="inputGroupPrepend" required>
        <option value="">@lang('site.team')</option>

        @foreach(\App\Models\Team::get() as $team)
            <option
                value="{{$team->id}} "{{@$plan->team_id == $team->id||old('team_id') == $team->id? 'selected' : '' }} >{{$team->name}}</option>
        @endforeach

    </select>
    <span class="invalid-feedback">@lang('site.required')</span>

    @error('team_id')<span class="text-danger">{{ $message }}</span>@enderror

</div>

<div class="form-group col-md-6">
    <label>@lang('site.sponsor')</label>
    <select data-placeholder="@lang('site.sponsor')" class="form-control basic" name="sponsor_id"
            aria-describedby="inputGroupPrepend" required>
        <option value="">@lang('site.sponsor')</option>

        @foreach(\App\Models\Sponsor::get() as $sponsor)
            <option
                value="{{$sponsor->id}} "{{@$plan->sponsor_id == $sponsor->id||old('sponsor_id') == $sponsor->id? 'selected' : '' }} >{{$sponsor->name}}</option>
        @endforeach

    </select>
    <span class="invalid-feedback">@lang('site.required')</span>

    @error('sponsor_id')<span class="text-danger">{{ $message }}</span>@enderror

</div>

<div class="form-group col-md-6">
    <label>@lang('site.camp_status')</label>
    <select data-placeholder="@lang('site.camp_status')" class="form-control basic" name="camp_status_id"
            aria-describedby="inputGroupPrepend" required>
        <option value="">@lang('site.camp_status')</option>

        @foreach(\App\Models\CampStatus::get() as $camp_status)
            <option
                value="{{$camp_status->id}} "{{@$plan->camp_status_id == $camp_status->id||old('camp_status_id') == $camp_status->id? 'selected' : '' }} >{{$camp_status->name}}</option>
        @endforeach

    </select>
    <span class="invalid-feedback">@lang('site.required')</span>

    @error('camp_status_id')<span class="text-danger">{{ $message }}</span>@enderror

</div>
<div class="form-group col-md-6">
    <label>@lang('site.sponsor_ship')</label>
    <select data-placeholder="@lang('site.sponsor_ship')" class="form-control basic" name="sponsor_ship_id"
            aria-describedby="inputGroupPrepend" required>
        <option value="">@lang('site.sponsor_ship')</option>

        @foreach(\App\Models\SponsorShip::get() as $sponsor_ship)
            <option
                value="{{$sponsor_ship->id}} "{{@$plan->sponsor_ship_id == $sponsor_ship->id||old('sponsor_ship_id') == $sponsor_ship->id? 'selected' : '' }} >{{$sponsor_ship->name}}</option>
        @endforeach

    </select>
    <span class="invalid-feedback">@lang('site.required')</span>

    @error('sponsor_ship_id')<span class="text-danger">{{ $message }}</span>@enderror

</div>

<!-- From Field -->
<div class="form-group col-sm-6">
    <label for="from">@lang('site.from')</label>
    {!! Form::date('from', @$plan->from, ['class' => 'form-control']) !!}
</div>

<!-- To Field -->
<div class="form-group col-sm-6">
    <label>@lang('site.to')</label>
    {!! Form::date('to', @$plan->to, ['class' => 'form-control']) !!}
</div>

<!-- Notes Field -->
<div class="form-group col-sm-10">
    <label>@lang('site.notes')</label>
    {!! Form::text('notes', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group " style="margin-left: -5px;margin-right: -5px">
    <label>@lang('site.need_follow_up')</label>
    <div class="col-sm-6">
        <label class="switch s-icons s-outline  s-outline-success  mb-8 mr-4">
            <input type="checkbox" name="need_follow_up" id="status" {{@$plan->need_follow_up==1?'checked':''}} >

            <span class="slider round"></span>
        </label>
    </div>
</div>

{{--<!-- Type Field -->--}}
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('type', 'Type:') !!}--}}
{{--    {!! Form::text('type', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}
