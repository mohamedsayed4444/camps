
<div class="table-responsive">
    <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
        <thead>
        <tr>
            <th>@lang('site.year')</th>
            <th>@lang('site.tour')</th>
            <th>@lang('site.country')</th>
            <th> @lang('site.camp')</th>
            <th> @lang('site.camp_status')</th>
            <th>@lang('site.team')</th>
            <th>@lang('site.sponsor')</th>
            <th>@lang('site.action')</th>
        </tr>
        </thead>
        <tbody>


        @foreach($plans as $index=>$plan)
            <tr id="row_{{$plan->id}}">
                <td class="text-center"><span class="td-content product-brand text-danger">{{ @$plan->year->name }}</span></td>
                <td class="text-center"><span class="td-content product-brand text-warning">{{ @$plan->tour->name }}</span></td>
                <td class="text-center"><span class="td-content product-brand text-warning">{{ @$plan->country->name }}</span></td>
                <td class="text-center"><span class="td-content product-brand text-primary">{{ @$plan->campStatus->name }}</span></td>
                <td class="text-center"><span class="td-content product-brand text-primary">{{ @$plan->camp->name }}</span></td>
                <td class="text-center"><span class="td-content product-brand text-danger">{{ @$plan->team->name }}</span></td>
                <td class="text-center"><span class="td-content product-brand text-danger">{{ @$plan->sponsor->name }}</span></td>

                <td class="text-center">

                    <ul class="table-controls">
                        <li><a href="{{ route('plans.show', $plan->id) }}" class="bs-tooltip"
                               title="@lang('site.show') ">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                     stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round"
                                     class="feather feather-eye text-danger">
                                    <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                    <circle cx="12" cy="12" r="3"></circle>
                                </svg>
                            </a></li>
                            <li><a href="{{ route('plans.edit', $plan->id) }}" class="bs-tooltip"
                                   title="@lang('site.edit') ">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                         stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round"
                                         class="feather feather-check-circle text-primary">
                                        <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                        <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                    </svg>
                                </a></li>
                            <li><a href="#"
                                   onclick="sweet_delete( ' {{ route('plans.destroy',$plan->id) }} ' , '{{ trans('site.confirm_delete') }}' ,{{ $plan->id }} )"
                                   class="bs-tooltip" title="@lang('site.delete') ">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                         stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round"
                                         class="feather feather-x-circle text-danger">
                                        <circle cx="12" cy="12" r="10"></circle>
                                        <line x1="15" y1="9" x2="9" y2="15"></line>
                                        <line x1="9" y1="9" x2="15" y2="15"></line>
                                    </svg>
                                </a></li>
                    </ul>


                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>

            <th>@lang('site.year')</th>
            <th>@lang('site.tour')</th>
            <th>@lang('site.country')</th>
            <th> @lang('site.camp')</th>
            <th> @lang('site.camp_status')</th>
            <th>@lang('site.team')</th>
            <th>@lang('site.sponsor')</th>
            <th style="visibility: hidden">#</th>

        </tr>
        </tfoot>
    </table>

</div>
