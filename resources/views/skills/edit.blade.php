@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <nav class="breadcrumb-two" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">@lang('site.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a
                                href="{{route('skills.index')}}">@lang('site.skills')</a></li>
                        <li class="breadcrumb-item "><a
                                href="{{route('skills.edit',$skill->id)}}">@lang('site.edit')</a></li>
                    </ol>
                </nav>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-primary float-right"
                   href="{{ route('skills.index') }}">
                    @lang('site.skill')
                </a>
            </div>
        </div>
    </div>


    <div class="content px-3 layout-top-spacing col-sm-12">

        {{--        @include('adminlte-templates::common.errors')--}}

        <div class="card">

            {!! Form::model($skill, ['route' => ['skills.update', $skill->id], 'method' => 'patch', 'files' => true,'class' =>"needs-validation " ,'novalidate']) !!}

            <div class="card-body">
                <div class="row">
                    @include('skills.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('skills.index') }}" class="btn btn-default">Cancel</a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
@section('scripts')
    @include('inc.file')
@endsection
