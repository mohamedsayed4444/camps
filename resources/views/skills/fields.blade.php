<!-- Nameen Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nameEn', __('site.nameEn')) !!}
    {!! Form::text('nameEn', null, ['class' => 'form-control','required'=>'required','aria-describedby'=>"inputGroupPrepend" ,'required']) !!}
    <span class="invalid-feedback">@lang('site.required')</span>
    @error('nameEn')<span class="text-danger">{{ $message }}</span>@enderror
</div>

<!-- Namear Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nameAr', __('site.nameAr')) !!}
    {!! Form::text('nameAr', null, ['class' => 'form-control','required'=>'required','aria-describedby'=>"inputGroupPrepend" ,'required']) !!}
    <span class="invalid-feedback">@lang('site.required')</span>
    @error('nameAr')<span class="text-danger">{{ $message }}</span>@enderror
</div>

<!-- Active Field -->
<div class="form-group col-sm-12">
    {!! Form::label('active', __('site.active')) !!}
    {!! Form::select('active', ['0' => __('site.inactive'), '1' => __('site.active')], null, ['class' => 'form-control custom-select','required'=>'required','aria-describedby'=>"inputGroupPrepend" ,'required']) !!}
    <span class="invalid-feedback">@lang('site.required')</span>
    @error('active')<span class="text-danger">{{ $message }}</span>@enderror
</div>
