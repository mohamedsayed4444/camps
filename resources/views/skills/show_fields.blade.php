<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $skill->id }}</p>
</div>

<!-- Nameen Field -->
<div class="col-sm-12">
    {!! Form::label('nameEn', 'Nameen:') !!}
    <p>{{ $skill->nameEn }}</p>
</div>

<!-- Namear Field -->
<div class="col-sm-12">
    {!! Form::label('nameAr', 'Namear:') !!}
    <p>{{ $skill->nameAr }}</p>
</div>

<!-- Active Field -->
<div class="col-sm-12">
    {!! Form::label('active', 'Active:') !!}
    <p>{{ $skill->active }}</p>
</div>

