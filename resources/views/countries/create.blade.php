@if (auth()->user()->hasPermission('read_users'))
    <li class="menu ">
        <a href="{{route('users.index')}}"
           aria-expanded="{{Request::is('*users*') ?'true':'false'}}"
           class="dropdown-toggle {{Request::is('users*') ?'':'collapse '}}">
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                     fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
                <span>@lang('site.users')</span>
            </div>

        </a>
    </li>
@endif
