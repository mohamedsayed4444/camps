<?php

return [
    'dashboard' => 'الرئيسيه',
    'logout' => 'تسجيل الخروج',
    'admin' => 'مدير الموقع',
    'related_images' => 'الصور المتعلقة',
    'add' => 'أضف جديد',
    'gallary' => 'الاستوديو',
    'photo' => 'صورة',
    'video' => 'فيديو',
    'customers' => 'كبار العملاء',
    'carts' => 'سلة المشتريات',
    'cart' => 'سلة المشتريات',
    'android_version' => 'نسخة أندرويد',
    'android_url' => 'رابط نسخة الأنرويد',
    'point' => 'النقاط',
    'mobile' => ' الموبايل',
    'mobile_verified' => 'تأكيد الموبايل',
    'email_verified' => 'تأكيد الايميل',
    'verified' => ' تم التاكيد',
    'not_verified' => 'لم يتم التاكيد',
    'page' => 'صفحة',
    'arabic' => 'عربي',
    'limit_of_usage' => 'مرات الاستخدام',
    'start_at' => 'تاريخ البدا',
    'english' => 'إنجليزي',

    'category_id'            => 'قائمة المنتجات ',
    'main_category'            => 'قائمة المنتجات الرئيسية ',
    'choose_main_category'            => 'اختر قائمة المنتجات الرئيسية ',
    'slug'            => 'الرابط ',
    'gallery'            => 'معرض الصور ',
    'user_name'            => 'اسم المستخدم ',
    'brand_id'            => 'ماركة ',
    'brand'            => 'ماركة ',
    'fees'            => 'رسوم ',
    '404'            => 'الصفحة غير موجودة ',
    'not_found'            => 'لا توجود بيانات متطابقة ',

    'back_to_home'            => 'الرجوع للرئيسية ',
    'cart_empty'            => ' سلة المشتريات فارغة ',
    'cart_empty_message'            => 'لاتمام عملية الشراء قم بالرجوع للرئيسية واضف المنتجات الي سلة المشتريات وتابع عملية الشراء ',
    'page_not_found_message'            => 'الصفحة التي تبحث عنها تم نقلها أو إزالتها أو إعادة تسميتها أو ربما لم تكن موجودة على الإطلاق. ',
    'page_not_found'            => 'عفوا! الصفحة المطلوبة لم يتم العثور على! ',
    'laratrust_forbidden_message'            => 'لا يمتلك المستخدم أيًا من حقوق الوصول لهذا الرابط. ',
    'brands'            => 'الماركات ',
    'coupons'            => 'اكواد الخصم ',
    'coupon'            => 'كود الخصم',
    'code'            => 'كود',
    'value'            => 'القيمة',
    'user'            => ' مستخديم النظام',
    'grand_total'            => 'المبلغ الكلي',
    'item_count'            => 'عدد العتاصر ',
    'is_paid'            => 'تم بيعه  ',
    'order_detail'            => 'تفاصيل الطب   ',
    'order_itemes'            => 'منتجات الطلب ',
    'confirm_order'            => 'تأكيد الطلب ',
    'confirm'            => 'تأكيد',
    'lowest_price'            => 'الأقل سعرا ',
    'online_payment'            => 'ادفع عبر الإنترنت ',
    'check_payment'            => 'الدفع عند الاستلام',
    'check_payment_detail'            => 'يرجى إرسال الشيك الخاص بك لاحظ أن هذه الخدمة متوفرة فقط في دولة الإمارات العربية المتحدة',
    'different_address'            => 'الشحن إلى عنوان مختلف؟ ',
    'payment_method'            => 'طريقة الدفع او السداد ',
    'shipping_address'            => 'عنوان الشحن',
    'shipping_state'            => 'دولة الشحن ',
    'shipping_zipcode'            => ' الرمز البريدي ',
    'shipping_phone'            => 'هاتف الشحن',
    'shipping_city'            => ' مدينة الشحن',
    'billing_fullname'            => 'الاسم بالكامل',
    'shipping_fullname'            => 'الاسم بالكامل',
    'billing_address'            => 'العنوان',
    'billing_city'            => 'المدينة ',
    'billing_state'            => 'الدولة ',
    'billing_zipcode'            => 'الرمز البريدي ',
    'billing_phone'            => 'الهاتف ',
    'order_item'            => 'عناصر الطلب ',
    'order_id'            => 'طلب',
    'price'            => 'مبلغ',
    'quantity'            => 'الكمية',
    'tag_id'            => 'كلمة دلالية',
    'product_tags'            => 'كلمات دلالية',
    'product_id'            => 'منتج',
    'tags'            => 'كلمات دلالية',
    'tag'            => 'كلمة دلالية',
    'Section'            => 'صفحة',
    'Sections'            => 'صفحلت',
    'title_en'            => 'العنوان بالانجبيزية',
    'body_ar'            => 'المحتوي بالعربية',
    'key_words_ar'            => ' الكلمات الدلالية بالعربيو',
    'title_ar'            => 'العنوان بالعربية',
    'body_en'            => 'المحتوي بالعربية',
    'key_words_en'            => 'الكلمات الدلالية بالانجليزية',
    'notes'            => 'الملاحظات',
    'message'            => 'الرسالة ',
    'confirm_password'            => 'تأكيد كلمة المرور ',
    'is_view'            => 'تم عرضة ',
    'view'            => 'عرض ',
    'send'            => 'إرسال ',
    'hot'            => ' ساخن',
    'new'            => 'جديد ',
    'special'            => 'عرض خاص ',
    'add_to_cart'            => 'إضافة  إلي سلة المشتريات ',
    'description_ar' => 'الوضف بالعربية',
    'name_ar' => 'الاسم بالعربية ',
    'description_en' => 'الوصف بالانجليزية',
    'name_en' => 'الاسم  بالانجليزية  ',
    'purchase_price'            => 'سعر الشراء ',
    'sale_price'            => ' سعر البيع ',
    'sold_item'            => ' المنتجات المباع ',
    'quantity'            => 'كمية',
    'not_available	'            => 'غير متاح',
    'men_product'            => 'ملابس رجالى',
    'method'            => 'طريقة الدفع',
    'women_product'            => 'ملابس حريمي',
    'review'            => 'تقييم ',
    'add_review'            => 'اضافة تقييم ',
    'related_product'            => 'منتجات ذات صلة ',
    'kids_product'            => 'ملابس أطفالي',
    'deleted_to_favorite'            => 'تم الحذف من المفضلة',

    'you_must_login'            => 'يجب عليك تسجيل الدخوا أولا',
    'click_here_to_login'            => 'اضغط هنا لتسجيل الدخول ',
    'register'            => 'إنشاء حساب',
    'size'            => 'حجم',
    'favorite'            => 'المفضلة',
    'subtotal'            => 'مبلغ إجمالي',
    'shipping'            => 'الشحن',
    'added_to_favorite'            => 'لقد تمت اضافته للمفضلة',
    'you_are_login'            => 'لقد قمت بتسجيل الدخول',
    'coupon_applied'            => 'تم تفعيل الكود',
    'Sorry_Coupon_does_not_exist'=>'القسيمة غير موجودة',
    'login_successfully'            => 'تم تسجيل الدخول بنجاح',
    'remove'            => 'حذف',
    'under_warrenty'            => 'المنتج غير خاضع للضمان',
    'apply_coupon'            => ' إشعار الخصم ',
    'free_shipping'            => 'الشحن مجاني',
    'hot_deal'            => 'صفقة اليوم ',
    'contact_info'            => ' معلومات الاتصال',
    'useful_links'            => 'روابط سريعة ',
    'new_arrival'            => ' وصلنا حديثا ',
    'best_seller'            => 'الافضل مبيعا ',
    'special_offer'            => 'عرض خاص   ',
    'warrenty'            => 'الضمان ',
    'collection'            => 'طلبات الجمله ',
    'more_category'            => 'عرض فئات اكثر',
    'pl'            => ' جنية  ',
    'xs'            => ' XS  ',
    'xl'            => ' XL  ',
    's'            => ' S  ',
    '2xl'            => ' 2XL  ',
    'm'            => ' M  ',
    'pl'            => ' PL  ',
    'pl'            => ' PL  ',
    'status'            => 'الحالة ',
    'status_time'            => 'حالة الوقت ',
    'featured'            => 'منتجات مميزة  ',
    'langs'            => 'الغة ',
    'logo'            => 'اللوجو ',
    'color'            => 'اللون ',
    'style'            => 'Style  ',
    'account_in_general'            => '  معلومات عامة',
    'account_detail'            => 'تفاصيل حسابي',
    'my_completed_order'            => 'طلاباتي المكتملة',
    'my_decline_order'            => 'طلبت تم الغاؤها',
    'my_pending_order'            => 'طلبات غير مكتملة',
    'completed_order_count'            => 'عدد الطلبات المكتملة',
    'decline_order_count'            => 'عدد الطلبات التي تم الغاؤها',
    'pending_order_count'            => 'عدد الطلبات غير المكتملة',
    'my_order'            => 'طلباتي',
    'favorite_product'            => 'منتجاتي المفضلة',
    'available'            => 'الصلاحية ',
    'home'            => 'الرئيسية ',
    'serach_for_product'            => 'بخث في المنتجات ',
    'show_more'            => 'عرض المزيد ',
    'all_category'            => 'كل الفئات ',
    'off'            => 'خصم ',
    'continue_shopping'            => 'الاستمرار في عملية الشراء ',
    'thanks_order'            => 'شكرا لطلبك! تتم معالجة طلبك وسيتم إكماله في غضون 3-6 ساعات. ستتلقى تأكيدًا عبر البريد الإلكتروني عند اكتمال طلبك. ',
    'your_order_is_completed!'            => 'تم الانتهاء من طلبك! ',
    'about_us'            => 'من نحن',
    'pages'            => ' الصفحات ',
    'pag'            => ' صفحة ',
    'view_cart'            => ' عرض سلة المشتريات ',
    'checkout'            => ' الدفع ',
    'my_account'            => 'حسابي ',
    'my_address'            => ' عنواني ',
    'my_favorite_product'            => ' منتجاتي المفضلة ',
    'contact_us'            => 'اتصل بنا',
    'all_categories'            => 'كل الفئات ',
    'create_account'            => ' عمل حسلب جديد',
    'new_password '            => 'الباسورد الجدبد   ',
    'not_have_account'            => 'لست لديك حساب   ',
    'do_you_have_account'            => 'هل لديك حساب ',
    'total'            => 'الاجمالي ',
    'filter'=>'ابحث من هنا',
    'our_brands'=>'علامتنا التجارية',
    'cart' => 'سلة المشتريات',
    'order' => 'طلب',
    'orders' => 'الطلبات',
    'facebook' => 'فيس بوك',
    'twitter' => 'تويتر',
    'address' => 'العنوان',
    'linkedin' => 'لينكد ان',
    'instagram' => 'انستغرام',
    'youtube' => 'يوتيوب',
    'phone' => 'رقم الموبايل',
    'tag' => 'كلمة استرشادية ',
    'tags' => ' الكلمات الإسترشادية ',
    'brand' => 'ماركة ',
    'brands' => 'الماركات ',
    'whats' => 'واتس ',
    'snap' => 'سناب شات ',
    'quality_assurance' => ' ضمان الجوده والشحن ',
    'quality_assurance_detail' => 'ما يتم اختياره من المنتجات من طرف العميل يتم شحنه والتاكد من وصوله للعميل فى وقت قياسى مع ضمان جوده المنتج والسعر التنافسى',
    'premium_products' => 'منتجات متميزة',
    'premium_products_detail' => ' تتميز منتاجتنا بالجوده العالية والسعر المناسب . مما يتح للعميل اشباع رغبتة التسويقية بأقل التكاليف',
    'speed​_of_communication' => 'سرعة التواصل',
    'speed_of_communication_detail' => ' فريق عمل يتميز بسرعة الرد على تلقى المشاكل وحلها فى اسرع وقت مما يجعل العميل فى حالة رضاء كامل من العميلة التسويقية داخل المتجر',
    'why_choose_us' => 'لماذا تختارنا',
    'lang' => 'اللغة',
    'pen' => 'بيتريست',
    'social' => 'موقع تواصل اجتماعي',
    'socials' => 'مواقع تواصل اجتماعي',
    'frequent_questions' => 'الاسئلة المتكررة',
    'questions' => 'الاسئلة',
    'question' => 'السؤال',
    'replay' => 'الجواب',
    'replies' => 'الاجوابة',
    'add_question' => 'أرسل سؤالا',
    'add_replay' => 'اضف رد',
    'product_detail' => 'تفاصيل المنتج',
    'Subscribe_Our_Newsletter' => 'إشترك في النشرة الاخبارية ',

    'comment' => 'التعليقات',
    'comments' => 'التعليقات',
    'activities' => 'الانشطة',
    'activity' => 'الانشطة',
    'active' => 'مفعل',
    'inActive' => 'غير مفعل',
    'packages' => 'الباقات',
    'package' => 'الباقات',
    'body' => 'المحتوي',
    'title' => 'العنوان',
    'post' => 'الخبر',
    'service' => 'خدمة',
    'services' => 'الخدمات',
    'our_services' => 'خدامتنا',
    'posts' => 'الاخبار',
    'create' => 'اضافه',
    'read' => 'عرض',
    'edit' => 'تعديل',
    'update' => 'تعديل',
    'delete' => 'حذف',
    'search' => 'بحث',
    'show' => 'عرض',
    'hidden' => 'مخفي',
    'loading' => 'جاري التحميل',
    'print' => 'طبع',
    'product' => 'منتج',
    'products' => 'المنتجات',
    'setting' => 'اعداد',
    'settings' => 'الأعدادات',
    'slider' => 'سليدر',
    'sliders' => 'سليدر',
    'contact' => 'اتصل بنا',
    'contacts' => 'اتصل بنا',
    'section' => 'الأجزاء ',
    'sections' => 'الأجزاء ',
    'lang' => 'اللغة ',
    'langs' => 'اللغات ',
    'logo' => 'الشعار ',
    'color' => 'اللون ',
    'style' => 'الشكل  ',
    'key_words' => 'الكلمات الدلالية ',

    'confirm_delete' => 'تاكيد الحذف',
    'pdf' => 'التحميل بضيغة كتاب',
    'excel' => 'اكسيل',
    'reset' => 'الرجوع',

    'reload' => 'اعادة تحميل',
    'yes' => 'نعم',
    'yes' => 'نعم',
    'no' => 'لا',

    'login' => 'تسجيل الدخول',
    'remember_me' => 'تذكرني',
    'password' => 'كلمه المرور',
    'password_confirmation' => 'تاكيد كلمه المرور',

    'added_successfully' => 'تم اضافه البيانات بنجاح',
    'updated_successfully' => 'تم تعديل البيانات بنجاح',
    'deleted_successfully' => 'تم حذف البيانات بنجاح',

    'no_data_found' => 'للاسف لا يوجد اي سجلات',
    'no_records' => 'للاسف لا يوجد اي سجلات',
    'is_show' => 'تم العرض',
    'not_readed' => 'غير مقروءة',
    'readed' => ' مقروءة',


    'users' => ' مستخديمين النظام',
    'first_name' => 'الاسم الاول',
    'last_name' => 'الاسم الاخير',
    'full_name' => 'الإسم بالكامل',
    'email' => 'الايميل ',
    'email_valid' => 'يجب ان يكون هذا الحقل ايميل  ',
    'password_valid' => ' كلمة السر يجب ان تكون اكبر من او يساوي 6',
    'valid_mobile' => 'يجب ادخال رقم موبايل صحيح .',
    'valid_url' => 'يجب ادخال رابط  صحيح .',
    'image' => 'صوره',
    'action' => 'اكشن',
    'blue' => 'أزرق',
    'orange' => 'برتقالي',
    'green' => 'أخضر',
    'red' => 'أحمر',
    'assets1' => 'الشكل الأول',
    'assets2' => 'الشكل الثاني',
    'assets3' => 'الشكل الثالت',

    'permissions' => 'الصلاحيات',

    'categories' => 'قوائم المنتجات',
    'name' => 'الاسم',
    'description' => 'الوصف',
    'category' => 'قائمة المنتجات',

    'created_at' => 'تاريخ االإضافة',
    'created_by' => ' أنشئت بواسطه',
    'url' => ' الرابط ',
    'type' => ' النوع ',
    'save' => ' حفظ ',
    'cancel' => ' الغاء ',
    'back' => ' الرجوع ',
    'branches' => 'الفروع ',
    'branch' => 'الفرع ',
    'shop_menu' => 'قائمة المشتريات ',
    'item' => 'قائمة المشتريات ',
    'items' => 'قوائم المشتريات ',
    'commission' => 'عمولة  ',
    'invoice' => 'فاتورة ضريبية  ',
    'revenues' => 'العائدات  ',
    'weasy_commission' => 'عمولة ويزي  ',
    'weasy_fees' => 'ضريبة ويزي  ',
    'weasy_tax' => 'رسوم ويزي  ',
    'fees_paid' => 'طريقة دفع الرسوم ',
    'order_number' => ' رقم الطلب ',

    'reservations' => 'الحجوزات',
    'reservation' => 'الحجز',
    'vendor' => 'المتجر',
    'vendors' => 'المتاجر',
    'longitude' => 'خط الطول',
    'latitude' => 'خط العرض',
    'has_ban' => 'خظر',
    'ban_reason' => 'سبب الحظر',
    'num_of_items' => ' لا يوجد عناصر ',
    'num_of_orders' => ' لا يوجد طلبات',
    'opening_time' => ' 	توقيت فتح المطعم',
    'closing_time' => '	توقيت غلق المطعم',
    'dnn_table' => 'طاولة مع طلب',
    'city' => 'المدينة',
    'cover' => 'صورة الغلاف',
    'day_off' => 'ايام الاجازات',
    'username' => 'اسم المستخدم',
    'contact_email' => 'ايميل المتصل',
    'contact_mobile' => 'رقم المتصل',
    'contact_name' => 'اسم المتصل',
    'bank_name' => 'اسم البنك',
    'iban_number' => 'رقم الاى بان',
    'account_holder_name' => 'اسم حامل الحساب',
    'fees_paid_type' => 'طريقة دفع الرسوم',
    'reservation_table_show' => 'عرض حجز الطاولة',
    'reservation_table_pay_cash' => 'رسوم حجز الطاولة كاش',
    'reservation_table_pay_online' => 'رسوم حجز الطاولة اونلين',
    'tax_register' => 'السجل الضريبي',
    'monthly' => 'شهري',
    'weekly' => 'اسبوعي',
    'normal_time' => 'الاوقات العادية',
    'rush_time' => 'أوقات الذروة',
    'extras' => 'الإضافات',
    'extra' => 'إضافة',
    'size_list' => 'الاحجام',
    'extra_list' => 'الاضافات',
    'works_with_days' => 'اوقات العمل',
    'vendor_type' => 'نوع المتجر',
    'limit_num_of_people' => 'حد عدد الافراد لحجز الطاولة',
    'customers' => 'العملاء',
    'customer' => 'عميل',
    'promocodes' => 'برومو كود',
    'promocode' => 'برومو كود',
    'discard' => 'إلغاء',
    'discount' => 'خصم',
    'expired_at' => '	تاريخ الانتهاء',
    'user_count' => 'مستخدمين البروموكود',
    'customer_name' => 'اسم العميل',
    'branch_name' => 'اسم الفرع',
    'vendor_name' => 'اسم المتجر',
    'total_price' => 'الإجمالي ',
    'sub_total' => 'إجمالي السعر',
    'interval_time ' => 'الوقت ',
    'offers' => 'العروض ',
    'offer' => 'عرض ',
    'start_date' => 'تاريخ البدء ',
    'end_date' => 'تاريخ البدء ',
    'points' => 'النقاط ',
    'index' => 'الكل ',
    'num_of_people' => 'عدد الاشخاص ',
    'date' => 'التاريخ ',
    'time' => 'الوقت ',
    'duration' => 'المدة ',
    'model' => 'الموديل ',
    'key' => 'المفتاح ',
    'event' => 'الفعل ',
    'order_ok' => 'تم الطلب ',
    'logs' => ' تفاعلات مستخدمي النظام ',
    'log' => ' تفاعل مستخدمي النظام ',
    'weasy' => 'ويزي ',
    'permissions_valid' => 'ليس لديك الحق في زيارة هذا الرابط',
    'super_admin' => 'مدير المشروع ',
    'operation' => 'المدير التنفيدي ',
    'customer_service' => 'خدمة العملاء ',
    'my_profile' => ' حسابي ',
    'log_out' => ' تسجيل خروج ',
    'complete_orders' => ' الطلبات المكتملة  ',
    'pending_orders' => ' الطلبات تحت الطلب ',
    'processing_orders' => ' الطلبات في مرحلة التجهيز',
    'processing' => '   مرحلة التجهيز',
    'process' => '   مرحلة التجهيز',
    'preparing_orders' => ' الطلبات في مرحلة التجهيز',
    'preparing' => '   مرحلة التجهيز',
    'preparing' => '   مرحلة التجهيز',
    'ready'=>'جاهز',
    'reject' => '    مرفوضة',
    'complete' => '    مكتملة',
    'pending' => '    تحت الطلب',
    'reject_orders' => '    الطلبات المرفوضة',
    'month' => '     شهر',
    'count' => '     العدد',
    'sum' => '     المجموع',
    'required' => '     هذا الحقل مطلوب',
    'interval_time' => '     الفترة المستغرقة',
    'top_selling_product' => '     المنجات الأكثر مبيعا',
    'recent_booking' => '  الحجوزات الحالية',
    'code_verification'=>'رمز التأكيد الخاص بكم هو ',
    'otp'=>'رمز التأكيد الخاص بكم هو ',
    'from'=>'من',
    'to'=>'إلي',
    'jeddah'=>'جدة',
    'riyadh'=>'الرياض',
    'dammam'=>'الدمام',
    'al_khobar'=>'الخبر',
    'activeImage'=>'الصوره  المفعلة',

    'data_retrieved' => ' تم استرجاعه بنجاح',
    'not_exist' => ' غير موجود ',
    'promocode_exist' => 'تم استخدام هذا الكود مسبقا',
    'promocode_usage' => 'تمت اضافة الكود بنجاح',
    'times' => 'مرات',
    'branch_item_exist' => 'Cart has Item From Other Branch you Must delete the Cart',
    'password_change_successfully' => 'تم تغيير كلمة المرور بنجاح ',
    'password_not_match' => 'كلمتا مرور غير متطابقتين',
    'reset_password' => 'إعادة تعيين كلمة المرور بنجاح',
    'wrong_code' => 'كود غير صالح ',
    'wrong_mobile_number' => 'رقم جوال خاطئ',
    'un_authorised' => 'غير مصرح',
    'your_special_code' => 'رمزك الخاص',
    'incorrect_password' => 'كلمة سر خاطئة',
    'empty_cart' => 'عربة فارغة',
    'error_occur' => 'حدث خطأ',
    'message_sent_Successfully' => 'تم إرسال الرسالة بنجاح',
    'verified_successfully' => 'تم التحقق بنجاح',
    'saved_successfully' => 'تم حفظ البيانات  بنجاح',
    'retrieved' => ' اسٌرجعت  بنجاح',
    'retrieved_successfully' => ' اسٌرجعت  بنجاح',
    'branch_not_in_work_time' => 'لا يمكن اتمام الطلب هذا الفرع لا يعمل الأن',
    'branch_busy' => 'لا يمكن اتمام الطلب هذا الفرع  ',
    'BUSY' => ' مشغول .',
    'CLOSED' => ' مغلق.',
    'hours' => '٢٤ ساعه',
    'is_seen' =>'رؤية الاشعار',
    'not_paid' =>'غير مدفوعة',
    'calories' => ' السعرات الحرارية.',
    'can_show_profit' => ' عرض الأرباح.',
    'country' => 'Country',
    'country_id' => 'دولة',
    'countries' => 'دول نتواجد فيها',
    'camp' => 'المخيمات',
    'camp_id' => 'المخيمات',
    'camps' => 'المخيمات',
    'camp_status' => 'حالات المخيم',
    'campStatus' => 'حالات المخيم',
    'campStatus_id' => 'حالة المخيمات',
    'follows' => 'المتابعات',
    'follow' => 'متابعة',
    'plans' => 'الخطط السنوية',
    'plan' => 'الخطة السنوية',
    'sponsors' => 'المتبرعين',
    'sponsor' => 'المتبرع',
    'sponsor_ships' => 'اسماء المتبرعين',
    'sponsorShips' => 'اسماء المتبرعين ',
    'sponsorShip' => 'اسماء المتبرعين ',
    'teams' => 'فرق العمل',
    'team' => 'فريق العمل',
    'tours' => 'جوالاتنا',
    'tour' => 'جولة',
    'years' => 'سنوات العطاء',
    'year' => 'سنوات العطاء',
    'need_follow_up' => 'في حاجة الي متابعة',
    'ending_date' => 'تاريخ الانتهاء',
    'save_changes' => 'حفظ التغيرات',
    'close' => 'اغلاق',
    'FinanceFollowUp' => 'المتابعات المالية ',
    'DrugsFollowUp' => 'متابعة الأدوية',
    'AdminFollowUp' => 'المتابعات الادارية  ',
    'MediaFollowUp' => 'المتابعات الاعلامية ة',
    'ar' => [
        'name' => 'الاسم بالغه العربيه',
        'description' => 'الوصف بالغه العربيه',
    ],

    'en' => [
        'name' => 'الاسم بالغه الانجليزيه',
        'description' => 'الوصف بالغه الانجليزيه',
    ],

];
