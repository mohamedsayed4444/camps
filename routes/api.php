<?php

Route::resource('course_categories', App\Http\Controllers\API\CourseCategoryAPIController::class);


Route::resource('tags', App\Http\Controllers\API\TagAPIController::class);


Route::resource('skills', App\Http\Controllers\API\SkillAPIController::class);


Route::resource('partners', App\Http\Controllers\API\PartnerAPIController::class);

Route::resource('courses', App\Http\Controllers\API\CourseAPIController::class);


Route::resource('trainers', App\Http\Controllers\API\TrainerAPIController::class);


Route::resource('programs', App\Http\Controllers\API\ProgramAPIController::class);


Route::resource('moderators', App\Http\Controllers\API\ModeratorAPIController::class);


Route::resource('articles', App\Http\Controllers\API\ArticleAPIController::class);


Route::resource('assessments', App\Http\Controllers\API\AssessmentAPIController::class);


Route::resource('requests', App\Http\Controllers\API\RequestAPIController::class);


Route::resource('countries', App\Http\Controllers\API\CountryAPIController::class);


Route::resource('years', App\Http\Controllers\API\YearAPIController::class);


Route::resource('camps', App\Http\Controllers\API\CampAPIController::class);


Route::resource('camp_statuses', App\Http\Controllers\API\CampStatusAPIController::class);


Route::resource('tours', App\Http\Controllers\API\TourAPIController::class);


Route::resource('teams', App\Http\Controllers\API\TeamAPIController::class);


Route::resource('sponsors', App\Http\Controllers\API\SponsorAPIController::class);


Route::resource('sponsor_ships', App\Http\Controllers\API\SponsorShipAPIController::class);


Route::resource('plans', App\Http\Controllers\API\PlanAPIController::class);


Route::resource('follows', App\Http\Controllers\API\FollowAPIController::class);
