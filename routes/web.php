<?php

use App\Models\Order;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\CampController;
use App\Http\Controllers\CampStatusController;
use App\Http\Controllers\SponsorController;
use App\Http\Controllers\SponsorShipController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TourController;
use App\Http\Controllers\YearController;

Route::get('/home', function () {
    return redirect()->route('dashboard');
});
Route::get('/', function () {
    return redirect()->route('dashboard');
});


Auth::routes();

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('lang/{locale}', [App\Http\Controllers\HomeController::class, 'setLanguage'])->name('setLanguage');

    Route::get('countries', [CountryController::class, 'index'])->name('countries.index');
    Route::post('add-update-country', [CountryController::class, 'store'])->name('add-update-country');
    Route::post('edit-country', [CountryController::class, 'edit'])->name('edit-country');
    Route::get('countries/{id}/destroy', [CountryController::class, 'destroy'])->name('countries.destroy');

    Route::get('camps', [CampController::class, 'index'])->name('camps.index');
    Route::post('add-update-camp', [CampController::class, 'store'])->name('add-update-camp');
    Route::post('edit-camp', [CampController::class, 'edit'])->name('edit-camp');
    Route::get('camps/{id}/destroy', [CampController::class, 'destroy'])->name('camps.destroy');

    Route::get('campStatus', [CampStatusController::class, 'index'])->name('campStatus.index');
    Route::post('add-update-CampStatus', [CampStatusController::class, 'store'])->name('add-update-campStatus');
    Route::post('edit-CampStatus', [CampStatusController::class, 'edit'])->name('edit-campStatus');
    Route::get('campStatus/{id}/destroy', [CampStatusController::class, 'destroy'])->name('campStatus.destroy');

    Route::get('sponsors', [SponsorController::class, 'index'])->name('sponsors.index');
    Route::post('add-update-sponsor', [SponsorController::class, 'store'])->name('add-update-sponsor');
    Route::post('edit-sponsor', [SponsorController::class, 'edit'])->name('edit-sponsor');
    Route::get('sponsors/{id}/destroy', [SponsorController::class, 'destroy'])->name('sponsors.destroy');
//
//
    Route::get('years', [YearController::class, 'index'])->name('years.index');
    Route::post('add-update-year', [YearController::class, 'store'])->name('add-update-year');
    Route::post('edit-year', [YearController::class, 'edit'])->name('edit-year');
    Route::get('years/{id}/destroy', [YearController::class, 'destroy'])->name('years.destroy');
//
    Route::get('tours', [TourController::class, 'index'])->name('tours.index');
    Route::post('add-update-tour', [TourController::class, 'store'])->name('add-update-tour');
    Route::post('edit-tour', [TourController::class, 'edit'])->name('edit-tour');
    Route::get('tours/{id}/destroy', [TourController::class, 'destroy'])->name('tours.destroy');
//
    Route::get('teams', [TeamController::class, 'index'])->name('teams.index');
    Route::post('add-update-team', [TeamController::class, 'store'])->name('add-update-team');
    Route::post('edit-team', [TeamController::class, 'edit'])->name('edit-team');
    Route::get('teams/{id}/destroy', [TeamController::class, 'destroy'])->name('teams.destroy');
//
//    Route::resource('logs', LogController::class);
//
//    Route::get('logs/{id}/destroy', [LogController::class, 'destroy'])->name('logs.destroy');
//
//    Route::resource('settings', App\Http\Controllers\SettingController::class);
    Route::resource('users', App\Http\Controllers\UserController::class);
//
    Route::get('sponsorShips', [SponsorShipController::class, 'index'])->name('sponsorShips.index');
    Route::post('add-update-sponsorShip', [SponsorShipController::class, 'store'])->name('add-update-sponsorShip');
    Route::post('edit-sponsorShip', [SponsorShipController::class, 'edit'])->name('edit-sponsorShip');
    Route::get('sponsorShips/{id}/destroy', [SponsorShipController::class, 'destroy'])->name('sponsorShips.destroy');

    Route::resource('plans', App\Http\Controllers\PlanController::class);
    Route::resource('follows', App\Http\Controllers\FollowController::class);


});



use App\Http\Controllers\BookController;
//
//Route::get('ajax-book-crud', [BookController::class, 'index']);
//Route::post('add-update-book', [BookController::class, 'store']);
//Route::post('edit-book', [BookController::class, 'edit']);
//Route::post('delete-book', [BookController::class, 'destroy']);
Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');
Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');
Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');
Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');
Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');
Route::post('generator_builder/generate-from-file', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile')->name('io_generator_builder_generate_from_file');




