<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class SponsorShip
 * @package App\Models
 * @version March 3, 2022, 12:55 pm UTC
 *
 * @property \App\Models\Sponsor $sponsor
 * @property string $name_ar
 * @property string $name_en
 * @property integer $user_id
 * @property integer $sponsor_id
 * @property boolean $status
 */
class SponsorShip extends Model
{
    use SoftDeletes;


    public $table = 'sponsor_ships';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name_ar',
        'name_en',
        'user_id',
        'sponsor_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name_ar' => 'string',
        'name_en' => 'string',
        'user_id' => 'integer',
        'sponsor_id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function sponsor()
    {
        return $this->belongsTo(\App\Models\Sponsor::class, 'sponsor_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function getNameAttribute()
    {
        if (app()->getLocale() == 'ar') {
            return $this->name_ar;
        } else {
            return $this->name_en;

        }

    }
}
