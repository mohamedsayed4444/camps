<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Tag
 * @package App\Models
 * @version December 20, 2021, 1:19 pm UTC
 *
 * @property string $NameEn
 * @property string $NameAr
 * @property string $Category
 */
class Tag extends Model
{

    use HasFactory;
    public $primaryKey  = 'Tag_ID';


    public $table = 'tags';

    public  $timestamps = false;



    public $guarded= [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'Tag_ID' => 'integer',
        'NameEn' => 'string',
        'NameAr' => 'string',
        'Category' => 'string'
    ];
    public function getIdAttribute()
    {
         return  $this->Tag_ID;
    }
    public function getNameAttribute()
    {
         return  app()->getLocale()=='en'? $this->NameEn : $this->NameAr;
    }
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
