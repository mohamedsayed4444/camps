<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Plan
 * @package App\Models
 * @version March 6, 2022, 1:15 am UTC
 *
 * @property \App\Models\Tour $tour
 * @property \App\Models\Year $year
 * @property \App\Models\Camp $camp
 * @property \App\Models\CampStatus $campStatus
 * @property \App\Models\Sponsor $sponsor
 * @property \App\Models\SponsorShip $sponsorShip
 * @property integer $year_id
 * @property integer $tour_id
 * @property integer $country_id
 * @property integer $camp_id
 * @property integer $team_id
 * @property integer $sponsor_id
 * @property integer $camp_status_id
 * @property integer $sponsor_ship_id
 * @property string $from
 * @property string $to
 * @property string $notes
 * @property boolean $need_follow_up
 * @property string $type
 */
class Plan extends Model
{
    use SoftDeletes;


    public $table = 'plans';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'year_id',
        'tour_id',
        'country_id',
        'camp_id',
        'team_id',
        'sponsor_id',
        'camp_status_id',
        'sponsor_ship_id',
        'from',
        'to',
        'notes',
        'need_follow_up',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'year_id' => 'integer',
        'tour_id' => 'integer',
        'country_id' => 'integer',
        'camp_id' => 'integer',
        'team_id' => 'integer',
        'sponsor_id' => 'integer',
        'camp_status_id' => 'integer',
        'sponsor_ship_id' => 'integer',
        'from' => 'date',
        'to' => 'date',
        'notes' => 'string',
        'need_follow_up' => 'boolean',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'year_id' => 'required',
        'tour_id' => 'required',
        'country_id' => 'required',
        'camp_id' => 'required',
        'team_id' => 'required',
        'sponsor_id' => 'required',
        'camp_status_id' => 'required',
        'sponsor_ship_id' => 'required',
        'from' => 'required',
        'to' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tour()
    {
        return $this->belongsTo(\App\Models\Tour::class, 'tour_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function year()
    {
        return $this->belongsTo(\App\Models\Year::class, 'year_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function camp()
    {
        return $this->belongsTo(\App\Models\Camp::class, 'camp_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function campStatus()
    {
        return $this->belongsTo(\App\Models\CampStatus::class, 'camp_status_id', 'id');
    }
    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class, 'country_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function sponsor()
    {
        return $this->belongsTo(\App\Models\Sponsor::class, 'sponsor_id', 'id');
    }
    public function team()
    {
        return $this->belongsTo(\App\Models\Team::class, 'team_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function sponsorShip()
    {
        return $this->belongsTo(\App\Models\SponsorShip::class, 'sponsor_ship_id', 'id');
    }
//    public function getNameAttribute()
//    {
//        if (app()->getLocale() == 'ar') {
//            return $this->name_ar;
//        } else {
//            return $this->name_en;
//
//        }
//
//    }
}
