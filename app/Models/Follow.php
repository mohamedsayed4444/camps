<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Follow
 * @package App\Models
 * @version March 8, 2022, 12:45 am UTC
 *
 * @property integer $camp_id
 * @property integer $camp_status_id
 * @property integer $ending_date
 * @property string $notes
 * @property boolean $status
 * @property string $files
 */
class Follow extends Model
{
    use SoftDeletes;


    public $table = 'follows';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'camp_id',
        'camp_status_id',
        'type',
        'ending_date',
        'notes',
        'status',
        'files'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'camp_id' => 'integer',
        'camp_status_id' => 'integer',

        'notes' => 'string',
        'status' => 'boolean',
        'files' => 'string'
    ];
    public function camp()
    {
        return $this->belongsTo(\App\Models\Camp::class, 'camp_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function campStatus()
    {
        return $this->belongsTo(\App\Models\CampStatus::class, 'camp_status_id', 'id');
    }
    public function getFilesPathAttribute()
    {
        return asset('public/uploads/' . $this->files);

        //   return asset(Storage::url($this->image));



    }
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
