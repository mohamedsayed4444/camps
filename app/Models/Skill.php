<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Skill
 * @package App\Models
 * @version December 20, 2021, 3:06 pm UTC
 *
 * @property string $nameEn
 * @property string $nameAr
 * @property integer $active
 */
class Skill extends Model
{

    use HasFactory;

    public $table = 'skill';
    public  $timestamps = false;




    public $fillable = [
        'nameEn',
        'nameAr',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nameEn' => 'string',
        'nameAr' => 'string',
        'active' => 'integer'
    ];
    public function getNameAttribute()
    {
        return  app()->getLocale()=='en'? $this->nameEn : $this->nameAr;
    }
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
