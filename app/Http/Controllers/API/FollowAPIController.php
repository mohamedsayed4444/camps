<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFollowAPIRequest;
use App\Http\Requests\API\UpdateFollowAPIRequest;
use App\Models\Follow;
use App\Repositories\FollowRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class FollowController
 * @package App\Http\Controllers\API
 */

class FollowAPIController extends AppBaseController
{
    /** @var  FollowRepository */
    private $followRepository;

    public function __construct(FollowRepository $followRepo)
    {
        $this->followRepository = $followRepo;
    }

    /**
     * Display a listing of the Follow.
     * GET|HEAD /follows
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $follows = $this->followRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($follows->toArray(), 'Follows retrieved successfully');
    }

    /**
     * Store a newly created Follow in storage.
     * POST /follows
     *
     * @param CreateFollowAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFollowAPIRequest $request)
    {
        $input = $request->all();

        $follow = $this->followRepository->create($input);

        return $this->sendResponse($follow->toArray(), 'Follow saved successfully');
    }

    /**
     * Display the specified Follow.
     * GET|HEAD /follows/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Follow $follow */
        $follow = $this->followRepository->find($id);

        if (empty($follow)) {
            return $this->sendError('Follow not found');
        }

        return $this->sendResponse($follow->toArray(), 'Follow retrieved successfully');
    }

    /**
     * Update the specified Follow in storage.
     * PUT/PATCH /follows/{id}
     *
     * @param int $id
     * @param UpdateFollowAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFollowAPIRequest $request)
    {
        $input = $request->all();

        /** @var Follow $follow */
        $follow = $this->followRepository->find($id);

        if (empty($follow)) {
            return $this->sendError('Follow not found');
        }

        $follow = $this->followRepository->update($input, $id);

        return $this->sendResponse($follow->toArray(), 'Follow updated successfully');
    }

    /**
     * Remove the specified Follow from storage.
     * DELETE /follows/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Follow $follow */
        $follow = $this->followRepository->find($id);

        if (empty($follow)) {
            return $this->sendError('Follow not found');
        }

        $follow->delete();

        return $this->sendSuccess('Follow deleted successfully');
    }
}
