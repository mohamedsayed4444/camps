<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateYearAPIRequest;
use App\Http\Requests\API\UpdateYearAPIRequest;
use App\Models\Year;
use App\Repositories\YearRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class YearController
 * @package App\Http\Controllers\API
 */

class YearAPIController extends AppBaseController
{
    /** @var  YearRepository */
    private $yearRepository;

    public function __construct(YearRepository $yearRepo)
    {
        $this->yearRepository = $yearRepo;
    }

    /**
     * Display a listing of the Year.
     * GET|HEAD /years
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $years = $this->yearRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($years->toArray(), 'Years retrieved successfully');
    }

    /**
     * Store a newly created Year in storage.
     * POST /years
     *
     * @param CreateYearAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateYearAPIRequest $request)
    {
        $input = $request->all();

        $year = $this->yearRepository->create($input);

        return $this->sendResponse($year->toArray(), 'Year saved successfully');
    }

    /**
     * Display the specified Year.
     * GET|HEAD /years/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Year $year */
        $year = $this->yearRepository->find($id);

        if (empty($year)) {
            return $this->sendError('Year not found');
        }

        return $this->sendResponse($year->toArray(), 'Year retrieved successfully');
    }

    /**
     * Update the specified Year in storage.
     * PUT/PATCH /years/{id}
     *
     * @param int $id
     * @param UpdateYearAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateYearAPIRequest $request)
    {
        $input = $request->all();

        /** @var Year $year */
        $year = $this->yearRepository->find($id);

        if (empty($year)) {
            return $this->sendError('Year not found');
        }

        $year = $this->yearRepository->update($input, $id);

        return $this->sendResponse($year->toArray(), 'Year updated successfully');
    }

    /**
     * Remove the specified Year from storage.
     * DELETE /years/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Year $year */
        $year = $this->yearRepository->find($id);

        if (empty($year)) {
            return $this->sendError('Year not found');
        }

        $year->delete();

        return $this->sendSuccess('Year deleted successfully');
    }
}
