<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCampStatusAPIRequest;
use App\Http\Requests\API\UpdateCampStatusAPIRequest;
use App\Models\CampStatus;
use App\Repositories\CampStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CampStatusController
 * @package App\Http\Controllers\API
 */

class CampStatusAPIController extends AppBaseController
{
    /** @var  CampStatusRepository */
    private $campStatusRepository;

    public function __construct(CampStatusRepository $campStatusRepo)
    {
        $this->campStatusRepository = $campStatusRepo;
    }

    /**
     * Display a listing of the CampStatus.
     * GET|HEAD /campStatuses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $campStatuses = $this->campStatusRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($campStatuses->toArray(), 'Camp Statuses retrieved successfully');
    }

    /**
     * Store a newly created CampStatus in storage.
     * POST /campStatuses
     *
     * @param CreateCampStatusAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCampStatusAPIRequest $request)
    {
        $input = $request->all();

        $campStatus = $this->campStatusRepository->create($input);

        return $this->sendResponse($campStatus->toArray(), 'Camp Status saved successfully');
    }

    /**
     * Display the specified CampStatus.
     * GET|HEAD /campStatuses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CampStatus $campStatus */
        $campStatus = $this->campStatusRepository->find($id);

        if (empty($campStatus)) {
            return $this->sendError('Camp Status not found');
        }

        return $this->sendResponse($campStatus->toArray(), 'Camp Status retrieved successfully');
    }

    /**
     * Update the specified CampStatus in storage.
     * PUT/PATCH /campStatuses/{id}
     *
     * @param int $id
     * @param UpdateCampStatusAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCampStatusAPIRequest $request)
    {
        $input = $request->all();

        /** @var CampStatus $campStatus */
        $campStatus = $this->campStatusRepository->find($id);

        if (empty($campStatus)) {
            return $this->sendError('Camp Status not found');
        }

        $campStatus = $this->campStatusRepository->update($input, $id);

        return $this->sendResponse($campStatus->toArray(), 'CampStatus updated successfully');
    }

    /**
     * Remove the specified CampStatus from storage.
     * DELETE /campStatuses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CampStatus $campStatus */
        $campStatus = $this->campStatusRepository->find($id);

        if (empty($campStatus)) {
            return $this->sendError('Camp Status not found');
        }

        $campStatus->delete();

        return $this->sendSuccess('Camp Status deleted successfully');
    }
}
