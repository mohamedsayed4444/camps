<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCampAPIRequest;
use App\Http\Requests\API\UpdateCampAPIRequest;
use App\Models\Camp;
use App\Repositories\CampRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CampController
 * @package App\Http\Controllers\API
 */

class CampAPIController extends AppBaseController
{
    /** @var  CampRepository */
    private $campRepository;

    public function __construct(CampRepository $campRepo)
    {
        $this->campRepository = $campRepo;
    }

    /**
     * Display a listing of the Camp.
     * GET|HEAD /camps
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $camps = $this->campRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($camps->toArray(), 'Camps retrieved successfully');
    }

    /**
     * Store a newly created Camp in storage.
     * POST /camps
     *
     * @param CreateCampAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCampAPIRequest $request)
    {
        $input = $request->all();

        $camp = $this->campRepository->create($input);

        return $this->sendResponse($camp->toArray(), 'Camp saved successfully');
    }

    /**
     * Display the specified Camp.
     * GET|HEAD /camps/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Camp $camp */
        $camp = $this->campRepository->find($id);

        if (empty($camp)) {
            return $this->sendError('Camp not found');
        }

        return $this->sendResponse($camp->toArray(), 'Camp retrieved successfully');
    }

    /**
     * Update the specified Camp in storage.
     * PUT/PATCH /camps/{id}
     *
     * @param int $id
     * @param UpdateCampAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCampAPIRequest $request)
    {
        $input = $request->all();

        /** @var Camp $camp */
        $camp = $this->campRepository->find($id);

        if (empty($camp)) {
            return $this->sendError('Camp not found');
        }

        $camp = $this->campRepository->update($input, $id);

        return $this->sendResponse($camp->toArray(), 'Camp updated successfully');
    }

    /**
     * Remove the specified Camp from storage.
     * DELETE /camps/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Camp $camp */
        $camp = $this->campRepository->find($id);

        if (empty($camp)) {
            return $this->sendError('Camp not found');
        }

        $camp->delete();

        return $this->sendSuccess('Camp deleted successfully');
    }
}
