<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSponsorShipAPIRequest;
use App\Http\Requests\API\UpdateSponsorShipAPIRequest;
use App\Models\SponsorShip;
use App\Repositories\SponsorShipRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SponsorShipController
 * @package App\Http\Controllers\API
 */

class SponsorShipAPIController extends AppBaseController
{
    /** @var  SponsorShipRepository */
    private $sponsorShipRepository;

    public function __construct(SponsorShipRepository $sponsorShipRepo)
    {
        $this->sponsorShipRepository = $sponsorShipRepo;
    }

    /**
     * Display a listing of the SponsorShip.
     * GET|HEAD /sponsorShips
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $sponsorShips = $this->sponsorShipRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($sponsorShips->toArray(), 'Sponsor Ships retrieved successfully');
    }

    /**
     * Store a newly created SponsorShip in storage.
     * POST /sponsorShips
     *
     * @param CreateSponsorShipAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSponsorShipAPIRequest $request)
    {
        $input = $request->all();

        $sponsorShip = $this->sponsorShipRepository->create($input);

        return $this->sendResponse($sponsorShip->toArray(), 'Sponsor Ship saved successfully');
    }

    /**
     * Display the specified SponsorShip.
     * GET|HEAD /sponsorShips/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SponsorShip $sponsorShip */
        $sponsorShip = $this->sponsorShipRepository->find($id);

        if (empty($sponsorShip)) {
            return $this->sendError('Sponsor Ship not found');
        }

        return $this->sendResponse($sponsorShip->toArray(), 'Sponsor Ship retrieved successfully');
    }

    /**
     * Update the specified SponsorShip in storage.
     * PUT/PATCH /sponsorShips/{id}
     *
     * @param int $id
     * @param UpdateSponsorShipAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSponsorShipAPIRequest $request)
    {
        $input = $request->all();

        /** @var SponsorShip $sponsorShip */
        $sponsorShip = $this->sponsorShipRepository->find($id);

        if (empty($sponsorShip)) {
            return $this->sendError('Sponsor Ship not found');
        }

        $sponsorShip = $this->sponsorShipRepository->update($input, $id);

        return $this->sendResponse($sponsorShip->toArray(), 'SponsorShip updated successfully');
    }

    /**
     * Remove the specified SponsorShip from storage.
     * DELETE /sponsorShips/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SponsorShip $sponsorShip */
        $sponsorShip = $this->sponsorShipRepository->find($id);

        if (empty($sponsorShip)) {
            return $this->sendError('Sponsor Ship not found');
        }

        $sponsorShip->delete();

        return $this->sendSuccess('Sponsor Ship deleted successfully');
    }
}
