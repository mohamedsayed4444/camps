<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SponsorShip;

class SponsorShipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sponsorShips = SponsorShip::orderBy('id','desc')->get();

        return view('sponsorShips.index',compact('sponsorShips'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $sponsorShip   =   SponsorShip::updateOrCreate(['id' => $request->id],
            [
                'name_ar' => $request->name_ar,
                'name_en' => $request->name_en,
                'sponsor_id' => $request->sponsor_id,
                'status' =>$request->status == 'on' ? 1 : 0,
                'user_id'=> auth()->id()
            ]);
        if ($sponsorShip) {
            session()->flash('success', __('site.saved_successfully'));
        }
        return response()->json(['success' => true]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $where = array('id' => $request->id);
        $sponsorShip  = SponsorShip::where($where)->first();
        $sponsorShip->setAttribute('sponsor_name',@$sponsorShip->sponsor->name);

        return response()->json($sponsorShip);
    }


    function destroy($id)
    {
        $sponsorShip = SponsorShip::find($id);

        $sponsorShip->delete();

        session()->flash('success', __('site.deleted_successfully'));
        return back();

    }//end of destroy
}
