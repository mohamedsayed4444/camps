<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Camp;

class CampController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $camps = Camp::orderBy('id','desc')->get();

        return view('camps.index',compact('camps'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $camp   =   Camp::updateOrCreate(['id' => $request->id],
            [
                'name_ar' => $request->name_ar,
                'name_en' => $request->name_en,
                'status' =>$request->status == 'on' ? true : false,
                'user_id'=> auth()->id(),
                'country_id'=> $request->country_id
            ]);
        if ($camp) {
            session()->flash('success', __('site.saved_successfully'));
        }
        return response()->json(['success' => true]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $where = array('id' => $request->id);
        $camp  = Camp::where($where)->first();
        $camp->setAttribute('country_name',@$camp->country->name);


        return response()->json($camp);
    }


    function destroy($id)
    {
        $camp = Camp::find($id);

        $camp->delete();

        session()->flash('success', __('site.deleted_successfully'));
        return back();

    }//end of destroy
}
