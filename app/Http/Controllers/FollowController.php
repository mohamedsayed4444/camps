<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFollowRequest;
use App\Http\Requests\UpdateFollowRequest;
use App\Repositories\FollowRepository;
use App\Http\Controllers\AppBaseController;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Flash;
use Response;

class FollowController extends AppBaseController
{
    /** @var  FollowRepository */
    private $followRepository;
use ImageTrait;
    public function __construct(FollowRepository $followRepo)
    {
        $this->followRepository = $followRepo;
    }

    /**
     * Display a listing of the Follow.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $follows = $this->followRepository->paginate(10);

        return view('follows.index')
            ->with('follows', $follows);
    }

    /**
     * Show the form for creating a new Follow.
     *
     * @return Response
     */
    public function create()
    {
        return view('follows.create');
    }

    /**
     * Store a newly created Follow in storage.
     *
     * @param CreateFollowRequest $request
     *
     * @return Response
     */
    public function store(CreateFollowRequest $request)
    {
        $input = $request->all();
   //     dd($request->all());
        if ($request->hasFile('files')) {
            $file_name = $this->savePDF( 'uploads',$request->file('files'));
            $input['files'] = $file_name;
        }
        $input['status'] = $request->status == 'on' ? 1 : 0;
        $follow = $this->followRepository->create($input);


        session()->flash('success', __('site.added_successfully'));

        return redirect(route('follows.index').'?type='.$follow->type);
    }

    /**
     * Display the specified Follow.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $follow = $this->followRepository->find($id);

        if (empty($follow)) {
            session()->flash('error', __('site.not_found'));

            return redirect(route('follows.index'));
        }

        return view('follows.show')->with('follow', $follow);
    }

    /**
     * Show the form for editing the specified Follow.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $follow = $this->followRepository->find($id);

        if (empty($follow)) {
            session()->flash('error', __('site.not_found'));

            return redirect(route('follows.index').'?type='.request('type'));
        }

        return view('follows.edit')->with('follow', $follow);
    }

    /**
     * Update the specified Follow in storage.
     *
     * @param int $id
     * @param UpdateFollowRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFollowRequest $request)
    {
        $follow = $this->followRepository->find($id);
        if (empty($follow)) {
            session()->flash('error', __('site.not_found'));

            return redirect(route('follows.index'));
        }
        $input = $request->all();
        if ($request->hasFile('files')) {
            $file_name = $this->savePDF( 'uploads',$request->file('files'));
            $input['files'] = $file_name;
        }
        $input['status'] = $request->status == 'on' ? 1 : 0;

        $follow = $this->followRepository->update($input, $id);

        session()->flash('success', __('site.updated_successfully'));

        return redirect(route('follows.index').'?type='.$follow->type);
    }

    /**
     * Remove the specified Follow from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $follow = $this->followRepository->find($id);

        if (empty($follow)) {
            session()->flash('error', __('site.not_found'));

            return redirect(route('follows.index'));
        }

        $this->followRepository->delete($id);

        session()->flash('success', __('site.deleted_successfully'));

        return redirect(route('follows.index'));
    }
}
