<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CourseCategory;
use App\Models\Follow;
use App\Models\Partner;
use App\Models\Program;
use App\Models\Skill;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function setLanguage($locale)
    {
        app()->setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    }

    public function dashboard()
    {

        $finance = Follow::where('type', 'FinanceFollowUp')->get();
        $drugs = Follow::where('type', 'DrugsFollowUp')->get();
        $admin = Follow::where('type', 'AdminFollowUp')->get();
        $media = Follow::where('type', 'MediaFollowUp')->get();
        $follows= Follow::select(DB::raw('YEAR(created_at) as year'), DB::raw('MONTH(created_at) as month'), DB::raw('count(*) as sum'))->groupBy('month')->get();
        $financeFollows= Follow::select(DB::raw('YEAR(created_at) as year'), DB::raw('MONTH(created_at) as month'), DB::raw('count(*) as sum'))->groupBy('month')->where('type','FinanceFollowUp')->get();
        $drugsFollows= Follow::select(DB::raw('YEAR(created_at) as year'), DB::raw('MONTH(created_at) as month'), DB::raw('count(*) as sum'))->groupBy('month')->where('type','DrugsFollowUp')->get();
        $adminFollows= Follow::select(DB::raw('YEAR(created_at) as year'), DB::raw('MONTH(created_at) as month'), DB::raw('count(*) as sum'))->groupBy('month')->where('type','AdminFollowUp')->get();
        $mediaFollows= Follow::select(DB::raw('YEAR(created_at) as year'), DB::raw('MONTH(created_at) as month'), DB::raw('count(*) as sum'))->groupBy('month')->where('type','MediaFollowUp')->get();

        return view('dashboard', compact('finance', 'drugs', 'admin', 'media','follows','financeFollows','drugsFollows','adminFollows','mediaFollows'));
    }
}
