<?php

namespace App\Repositories;

use App\Models\SponsorShip;
use App\Repositories\BaseRepository;

/**
 * Class SponsorShipRepository
 * @package App\Repositories
 * @version March 3, 2022, 12:55 pm UTC
*/

class SponsorShipRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_ar',
        'name_en',
        'user_id',
        'sponsor_id',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SponsorShip::class;
    }
}
