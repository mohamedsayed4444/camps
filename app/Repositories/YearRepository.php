<?php

namespace App\Repositories;

use App\Models\Year;
use App\Repositories\BaseRepository;

/**
 * Class YearRepository
 * @package App\Repositories
 * @version March 3, 2022, 12:15 pm UTC
*/

class YearRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_ar',
        'name_en',
        'user_id',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Year::class;
    }
}
