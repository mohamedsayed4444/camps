<?php

namespace App\Repositories;

use App\Models\Course;
use App\Repositories\BaseRepository;

/**
 * Class CourseRepository
 * @package App\Repositories
 * @version December 21, 2021, 12:09 pm UTC
*/

class CourseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Title',
        'TitleArabic',
        'Description',
        'DescriptionArabic',
        'LearningObjectives',
        'LearningObjectivesArabic',
        'SkillsGained',
        'SkillsGainedArabic',
        'Category',
        'Language',
        'Level',
        'StartTime',
        'EndTime',
        'Color',
        'IsActive',
        'InstructorEmail',
        'image',
        'MeetingURL',
        'VideoId',
        'Type',
        'Location',
        'Duration',
        'Material',
        'CourseProviderImage',
        'Tags'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Course::class;
    }
}
