<?php

namespace App\Repositories;

use App\Models\Request;
use App\Repositories\BaseRepository;

/**
 * Class RequestRepository
 * @package App\Repositories
 * @version January 7, 2022, 8:09 am UTC
*/

class RequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Request::class;
    }
}
