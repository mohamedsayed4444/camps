<?php

namespace App\Repositories;

use App\Models\Content;
use App\Repositories\BaseRepository;

/**
 * Class ContentRepository
 * @package App\Repositories
 * @version September 23, 2021, 9:03 am UTC
*/

class ContentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image',
        'type',
        'title_ar',
        'title_en',
        'body_ar',
        'body_en'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Content::class;
    }
}
