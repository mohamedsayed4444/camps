<?php

namespace App\Repositories;

use App\Models\Moderator;
use App\Repositories\BaseRepository;

/**
 * Class ModeratorRepository
 * @package App\Repositories
 * @version December 27, 2021, 9:24 am UTC
*/

class ModeratorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Moderator::class;
    }
}
