<?php

namespace App\Repositories;

use App\Models\CampStatus;
use App\Repositories\BaseRepository;

/**
 * Class CampStatusRepository
 * @package App\Repositories
 * @version March 3, 2022, 12:36 pm UTC
*/

class CampStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_ar',
        'name_en',
        'user_id',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CampStatus::class;
    }
}
