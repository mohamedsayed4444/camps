<?php

namespace App\Repositories;

use App\Models\Assessment;
use App\Repositories\BaseRepository;

/**
 * Class AssessmentRepository
 * @package App\Repositories
 * @version December 28, 2021, 9:42 am UTC
*/

class AssessmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Questions',
        'Answers',
        'CorrectAnswers',
        'Type',
        'Course_ID'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Assessment::class;
    }
}
