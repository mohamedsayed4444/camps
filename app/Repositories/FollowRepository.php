<?php

namespace App\Repositories;

use App\Models\Follow;
use App\Repositories\BaseRepository;

/**
 * Class FollowRepository
 * @package App\Repositories
 * @version March 8, 2022, 12:45 am UTC
*/

class FollowRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'camp_id',
        'camp_status_id',
        'ending_date',
        'notes',
        'status',
        'files'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Follow::class;
    }
}
