<?php

namespace App\Repositories;

use App\Models\PublicQuestion;
use App\Repositories\BaseRepository;

/**
 * Class PublicQuestionRepository
 * @package App\Repositories
 * @version September 20, 2021, 7:48 pm UTC
*/

class PublicQuestionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'question_ar',
        'question_en',
        'answer_ar',
        'answer_en'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PublicQuestion::class;
    }
}
