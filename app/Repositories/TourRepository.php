<?php

namespace App\Repositories;

use App\Models\Tour;
use App\Repositories\BaseRepository;

/**
 * Class TourRepository
 * @package App\Repositories
 * @version March 3, 2022, 12:44 pm UTC
*/

class TourRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_ar',
        'anme_en',
        'user_id',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tour::class;
    }
}
