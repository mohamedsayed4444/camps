<?php

namespace App\Repositories;

use App\Models\Branch;
use App\Repositories\BaseRepository;

/**
 * Class BranchRepository
 * @package App\Repositories
 * @version September 20, 2021, 7:26 pm UTC
*/

class BranchRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'vendor_id',
        'name_ar',
        'name_en',
        'latitude',
        'longitude',
        'opening_time',
        'closing_time',
        'status',
        'availale',
        'dnn_table',
        'mobile_code',
        'mobile',
        'phone',
        'rates',
        'num_of_items',
        'num_of_orders',
        'total_earnings'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Branch::class;
    }
}
