<?php

namespace App\Repositories;

use App\Models\Promocode;
use App\Repositories\BaseRepository;

/**
 * Class PromocodeRepository
 * @package App\Repositories
 * @version September 20, 2021, 10:28 pm UTC
*/

class PromocodeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'discount',
        'type',
        'expire_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Promocode::class;
    }
}
