<?php

namespace App\Repositories;

use App\Models\CourseCategory;
use App\Repositories\BaseRepository;

/**
 * Class CourseCategoryRepository
 * @package App\Repositories
 * @version December 20, 2021, 11:47 am UTC
*/

class CourseCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'titleArabic',
        'description',
        'descriptionArabic',
        'active',
        'type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CourseCategory::class;
    }
}
