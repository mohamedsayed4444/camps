<?php

namespace App\Repositories;

use App\Models\Trainer;
use App\Repositories\BaseRepository;

/**
 * Class TrainerRepository
 * @package App\Repositories
 * @version December 22, 2021, 10:27 am UTC
*/

class TrainerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'name',
        'email',
        'contact',
        'bio',
        'photo',
        'organization',
        'partner',
        'coordinator',
        'status',
        'password'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Trainer::class;
    }
}
