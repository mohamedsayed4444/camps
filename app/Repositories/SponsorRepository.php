<?php

namespace App\Repositories;

use App\Models\Sponsor;
use App\Repositories\BaseRepository;

/**
 * Class SponsorRepository
 * @package App\Repositories
 * @version March 3, 2022, 12:48 pm UTC
*/

class SponsorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_ar',
        'name_en',
        'user_id',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sponsor::class;
    }
}
