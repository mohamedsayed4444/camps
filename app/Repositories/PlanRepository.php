<?php

namespace App\Repositories;

use App\Models\Plan;
use App\Repositories\BaseRepository;

/**
 * Class PlanRepository
 * @package App\Repositories
 * @version March 6, 2022, 1:15 am UTC
*/

class PlanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'year_id',
        'tour_id',
        'country_id',
        'camp_id',
        'team_id',
        'sponsor_id',
        'camp_status_id',
        'sponsor_ship_id',
        'from',
        'to',
        'notes',
        'need_follow_up',
        'type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Plan::class;
    }
}
