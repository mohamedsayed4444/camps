<?php

namespace App\Repositories;

use App\Models\Vendor;
use App\Repositories\BaseRepository;

/**
 * Class VendorRepository
 * @package App\Repositories
 * @version September 20, 2021, 7:02 pm UTC
*/

class VendorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_ar',
        'name_en',
        'category_id',
        'email',
        'mobile_code',
        'mobile',
        'phone',
        'facebook_url',
        'latitude',
        'longitude',
        'has_ban',
        'ban_reason',
        'image',
        'cover',
        'wallet',
        'rates',
        'num_of_items',
        'num_of_orders',
        'total_earnings',
        'available',
        'status',
        'display_in_slider',
        'created_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vendor::class;
    }
}
