<?php

namespace App\Repositories;

use App\Models\Camp;
use App\Repositories\BaseRepository;

/**
 * Class CampRepository
 * @package App\Repositories
 * @version March 3, 2022, 12:21 pm UTC
*/

class CampRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_ar',
        'name_en',
        'user_id',
        'country_id',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Camp::class;
    }
}
